#!/usr/bin/env zsh

function themekit-check() {
  b=$(tput bold)
  c=$(tput sgr0)

  PACKAGE="Shopify Themekit"

  if ! [ -x "$(command -v theme)" ]; then
    echo "\n🛑  ${b}${PACKAGE} is not installed."
    echo "\n☕️  ${b}Installing ${PACKAGE}. Go make yourself a brew because this may take a few minutes..."

    curl -s https://shopify.github.io/themekit/scripts/install.py | sudo python

    echo "\n✅  ${b}${PACKAGE} is installed. Please re-run script."
    exit
  else
    echo "🥂  ${b}Downloading ${1} theme files into src directory."
  fi
}
