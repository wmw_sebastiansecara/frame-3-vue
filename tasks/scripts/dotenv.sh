#!/bin/bash

function dotenv() {
  set -a
  [[ -f .env.${1} ]] && source .env.${1}
  set +a
}