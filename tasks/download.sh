#!/bin/bash

b=$(tput bold)
c=$(tput sgr0)

DIR="./src/"
BASE="./tasks/scripts/"
ENVIRONMENTS=( .env.* )
ENVIRONMENTS=( "${ENVIRONMENTS[@]##*/}" )
NUM_ARGS=1

source $BASE/dotenv.sh
source $BASE/themekit-check.sh

show_usage() {
  echo "\n⚠️  ${b}Missing environment argument"

  echo "\n${c}Usage: yarn download <environment>

<environment> is the name of the environment to download into your src directory
This is the suffix of your .env file you are attempting to download.

Available environments:
`( IFS=$'\n'; echo "${ENVIRONMENTS[*]}" )`

Examples:
  yarn download sample
  yarn download production
  yarn download development
"
}

[[ $# -ne $NUM_ARGS || $1 = -h ]] && { show_usage; exit 0; }

# Read .env file from parameter
dotenv $1

# Check if Shopify Themekit is installed before running command
themekit-check $1

# Download theme files from directory using Shopify Themekit
if [[ $SLATE_PASSWORD || $SLATE_THEME_ID || $SLATE_STORE ]]; then
  theme download --password=$SLATE_PASSWORD --themeid=$SLATE_THEME_ID --store=$SLATE_STORE --dir=${DIR} --ignores=".frameignore"
else
  echo "\n🔴  Unable to find .env. Please check values."
fi
