#!/bin/bash

AUDITS="./audits/"
CONFIG="./tasks/lighthouse/lighthouse-config.js"
ENVIRONMENT=( .env.development )
REPORT_EXTENSION=".report.html"
source $ENVIRONMENT

PREVIEW_ID="?preview_theme_id=$SLATE_THEME_ID"

! hash lighthouse &>/dev/null && echo "💡 \e[33mInstalling lighthouse..." && yarn global add lighthouse

if [ ! -d "$AUDITS" ]; then
  mkdir "$AUDITS" 
fi

if [ "$2" = "--core" ]; then

    echo "\e[92mRunning homepage audit..."
    lighthouse "https://$SLATE_STORE$PREVIEW_ID" --config-path=$CONFIG --output-path "$AUDITS"index"$REPORT_EXTENSION" --throttling-method=simulate 

    echo "\e[92mRunning collection page audit..."
    lighthouse "https://$SLATE_STORE$COLLECTION$PREVIEW_ID" --config-path=$CONFIG --output-path "$AUDITS"collection"$REPORT_EXTENSION" --throttling-method=simulate 

    echo "\e[92mRunning product page audit..."
    lighthouse "https://$SLATE_STORE$PRODUCT$PREVIEW_ID" --config-path=$CONFIG --output-path "$AUDITS"product"$REPORT_EXTENSION" --throttling-method=simulate 

    echo -e "\e[93mCore pages audit finished 👍"

    open "$AUDITS""index""$REPORT_EXTENSION"
    open "$AUDITS""collection""$REPORT_EXTENSION"
    open "$AUDITS""product""$REPORT_EXTENSION"

else

    echo "\e[92mPlease enter the relative path you want to audit on the site"
    read ROUTE

    if [ -z "$ROUTE" ]; then
        ROUTE="index"
    fi

    if [[ $ROUTE =~ ^/ ]]; then
        ROUTE="${ROUTE:1}"
    fi

    SANITIZED_ROUTE=${ROUTE//\//_}
    SANITIZED_ROUTE=${SANITIZED_ROUTE//-/_}


    if [ -e "$AUDITS$SANITIZED_ROUTE$REPORT_EXTENSION" ]; then
        KEY_WORD="updated"
    else
        KEY_WORD="created"
    fi

    if [ "$2" = "--dev" ]; then
        echo "\e[93mRunning audit on your development theme..."
        lighthouse "https://$SLATE_STORE/$ROUTE$PREVIEW_ID" --config-path=$CONFIG --output-path "$AUDITS$SANITIZED_ROUTE$REPORT_EXTENSION" --throttling-method=simulate 
    else
        echo "\e[93mRunning audit on the live theme..."
        lighthouse "https://$SLATE_STORE/$ROUTE" --config-path=$CONFIG --output-path "$AUDITS$SANITIZED_ROUTE$REPORT_EXTENSION"  --throttling-method=simulate 
    fi
    


    echo -e "Audit finished for" "\e[92mhttps://$SLATE_STORE/$ROUTE$PREVIEW_ID"
    echo -e "\e[33m$AUDITS$SANITIZED_ROUTE$REPORT_EXTENSION" "\e[39mhas been $KEY_WORD 👍"

    open "$AUDITS$SANITIZED_ROUTE$REPORT_EXTENSION"
fi
