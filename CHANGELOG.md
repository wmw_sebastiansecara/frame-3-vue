# Change log
---

## v1.0.2 (19-12-2018)
#### 🐞 Bug Fix
* `payment-icons.scss`, `theme.scss`, `site-footer.liquid`, `cart.liquid`, `cart-table.scss`
	* [c0ec984](https://bitbucket.org/we-make-websites/frame-3/commits/c0ec9841b6a196f47b7b2eb97a880ff8b3fcb600) Updated base styling of payment icons. [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)
* `package.json`, `cart-drawer-js`, `menu-drawer.js`
	* [1d57cae](https://bitbucket.org/we-make-websites/frame-3/commits/1d57cae4c67de14992ef1a52a1fa85ffbc53394b) Added body scroll lock to cart and menu drawers. [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)

####  🚀 Enhancement
* `quick-view.js`, `modal-quick-view.liquid`, `quick-view.scss`, `product.quick-view.liquid`
	*  [db5b3cd](https://bitbucket.org/we-make-websites/frame-3/commits/db5b3cdf706e136cfb566cf15b4f9664c72977f4) Added Quick View component to product card grids. Documentation on implementation and how-to has been updated on Frame 3 Features  on Slite. [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)
* `package.json`, `build.sh`, `sslcheck.sh`
	* [665cb9a](https://bitbucket.org/we-make-websites/frame-3/commits/665cb9afddd09013f71501bf50a725973638f88c)  Added SSL bash script to check ssl certificate on start:dev script. [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)

#### Committers: 1
* Daniel Ngo [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)

## v1.0.1 (22-11-2018)
#### 🐞 Bug Fix
* `site-header.liquid`, `site-header.scss`
	* [#4](https://bitbucket.org/we-make-websites/frame-3/pull-requests/4/added-a-submit-button-to-header-search) Added submit button to search toggle in site header for accessibility. [@wmw_adamsmith](https://bitbucket.org/wmw_adamsmith/)
* `accordion.js`
	* [256cccd](https://bitbucket.org/we-make-websites/frame-3/commits/256cccd0761cf4fd7cecaaa07314364ba792428a) Removed click event from accordion body causing the component to close. [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)
	
####  🚀 Enhancement
* `.env.sample`
	* Added default sample environment file to project directory to hold store API values [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)
* `product-form.js`, `theme.js`, `theme-strings.liquid`
	* [b285d44](https://bitbucket.org/we-make-websites/frame-3/commits/b285d44a1f11663c6fa92d6b5e076af3dad8d587) Added feature toggling variables in theme to prevent inactive scripts from initialising. [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)
* `accordion.js`, `styleguide.js`, `styleguide-components.liquid`
	* [014ff93](https://bitbucket.org/we-make-websites/frame-3/commits/014ff93c0e28bd69ac718c1d4c60a5f2ba78cda4) Extended accordion component to support initial item option in configuration. [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)

#### Committers: 2
* Daniel Ngo [@wmw_danielngo](https://bitbucket.org/wmw_danielngo)
* Adam Smith [@wmw_adamsmith](https://bitbucket.org/wmw_adamsmith/)