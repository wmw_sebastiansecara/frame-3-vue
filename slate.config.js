/* eslint-disable no-undef */

const path = require('path');
const {VueLoaderPlugin} = require('vue-loader');

const alias = {
  'lodash-es': path.resolve('./node_modules/lodash-es'),
  vue: 'vue/dist/vue.js',
};

module.exports = {
  slateTools: {
    extends: {
      dev: {
        resolve: {
          alias,
        },
      },
      prod: {
        resolve: {
          alias,
        },
      },
    },
  },
  'webpack.extend': {
    resolve: {
      alias,
      extensions: ['.js', '.css', '.json', '.vue'],
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
        },
        {
          test: /.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
        },
      ],
    },
    plugins: [
      new VueLoaderPlugin(),
    ],
  },
};
