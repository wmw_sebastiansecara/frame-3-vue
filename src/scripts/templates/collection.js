/**
 * Template: Collection
 * ------------------------------------------------------------------------------
 * Highly coupled scripts to power the collection page functionality
 *
 * @namespace collection
 */
import 'url-search-params-polyfill';

import breakpoints from '../helpers/breakpoints';
import Accordion from '../components/accordion';
import Toggle from '../components/toggle';
import SortBy from '../components/sort-by';
import QuickView from '../components/quick-view';

/**
 * Instantiate collection modules.
 */
SortBy().init();

Toggle({
  toggleSelector: 'filterToggle',
  overlay: false,
  toggleTabIndex: false,
}).init();

Accordion('.accordion', {
  singleOpen: false,
  showAll: false,
  responsive: [
    {
      breakpoint: breakpoints.medium,
      settings: {
        showAll: true,
      },
    },
  ],
}).init();

if (theme.features.quickview) {
  QuickView().init();
}
