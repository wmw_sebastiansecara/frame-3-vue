/**
 * Template: Index
 * -----------------------------------------------------------------------------
 * Scripts to execute on the index page.
 *
 */
import {load} from '@shopify/theme-sections';

import '../sections/image-slider';
import '../sections/newsletter-modal';
import '../sections/featured-collection';

document.addEventListener('DOMContentLoaded', () => {
  load('*');
});
