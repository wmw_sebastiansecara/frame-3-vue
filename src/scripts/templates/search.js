/**
 * Template: Search
 * ------------------------------------------------------------------------------
 * Highly coupled scripts to power the search page functionality
 *
 * @namespace search
 */
import QuickView from '../components/quick-view';

if (theme.features.quickview) {
  QuickView().init();
}
