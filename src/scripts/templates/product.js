/**
 * Template: Product
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */
import {load} from '@shopify/theme-sections';

import Accordion from '../components/accordion';
import ProductForm from '../components/product-form';
import ProductGallery from '../components/product-gallery';
import '../sections/recommended-products';

document.addEventListener('DOMContentLoaded', () => {
  load('recommended-products');

  ProductForm({
    container: 'container',
    enableSwatches: true,
  }).init();

  ProductGallery().init();
  Accordion('.accordion', {
    singleOpen: false,
  }).init();
});
