/**
 * Template: Customer addresses
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Customer Addresses
 * template.
 *
 * @namespace customerAddresses
 */
import cssClasses from '../../helpers/cssClasses';
import {on} from '../../helpers/utils';

export function AddressForm() {

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    newAddressForm: document.querySelector('[js-addresses="newForm"]'),
    addressNewToggle: document.querySelectorAll('[js-addresses="newFormToggle"]'),
    addressEditToggle: document.querySelectorAll('[js-addresses="editAddress"]'),
    addressDeleteToggle: document.querySelectorAll('[js-addresses="deleteAddress"]'),
    countryOptions: document.querySelectorAll('[js-addresses="countryOption"]'),
    contentToggle: document.querySelectorAll('[js-addresses="contentToggle"]'),
    addressCheckboxes: document.querySelectorAll('[js-addresses="checkbox"] input'),
  };

  /**
   * Initialize address form events.
   */
  function init() {
    setToggleEvents();
    addCheckboxClass();
  }

  function setToggleEvents() {
    [...nodeSelectors.addressNewToggle].forEach((element) => {
      on('click', element, (event) => handleFormToggle(event));
    });

    [...nodeSelectors.addressEditToggle].forEach((element) => {
      on('click', element, (event) => handleEditToggle(event));
    });

    [...nodeSelectors.addressDeleteToggle].forEach((element) => {
      on('click', element, (event) => handleDelete(event));
    });

    if (nodeSelectors.newAddressForm) {
      if (Shopify) {
        initializeObservers();
      }

      initialiseEditObservers();
    }

    /**
     * Initialize each edit form's country/province selector.
     */
    function initialiseEditObservers() {
      [...nodeSelectors.countryOptions].forEach((element) => {
        const formId = element.dataset.formId;
        const countrySelector = `AddressCountry_${formId}`;
        const provinceSelector = `AddressProvince_${formId}`;
        const containerSelector = `AddressProvinceContainer_${formId}`;

        new Shopify.CountryProvinceSelector(countrySelector, provinceSelector, {
          hideElement: containerSelector,
        });
      });
    }

    /**
     * Toggle address form.
     */
    function handleFormToggle(event) {
      event.preventDefault();
      nodeSelectors.newAddressForm.classList.toggle(cssClasses.hidden);
      togglePageContent();
    }

    /**
     * Handle edit address toggle on each for element.
     * @param {Event} event - Click event.
     */
    function handleEditToggle(event) {
      const target = event.currentTarget;
      const formId = target.dataset.formId;

      document.querySelector(`#EditAddress_${formId}`).classList.toggle(cssClasses.hidden);
      togglePageContent();
    }

    /**
     * Toggle page content when edit/add form is visible.
     */
    function togglePageContent() {
      [...nodeSelectors.contentToggle].forEach((element) => {
        element.classList.toggle(cssClasses.hidden);
      });
    }

    /**
     * Handle delete on address.
     */
    function handleDelete(event) {
      const target = event.currentTarget;
      const formId = target.dataset.formId;
      const confirmMessage = target.dataset.confirmMessage;

      if (
        window.confirm(
          confirmMessage || 'Are you sure you wish to delete this address?',
        )
      ) {
        Shopify.postLink(`/account/addresses/${formId}`, {
          parameters: {_method: 'delete'},
        });
      }
    }

    /**
     * Initialize observers on address selectors, defined in shopify_common.js.
     */
    function initializeObservers() {
      new Shopify.CountryProvinceSelector(
        'AddressCountryNew',
        'AddressProvinceNew',
        {
          hideElement: 'AddressProvinceContainerNew',
        },
      );
    }
  }

  /**
   * Add custom class to checkbox form elements.
   */
  function addCheckboxClass() {
    [...nodeSelectors.addressCheckboxes].forEach((element) => {
      element.classList.add('checkbox__input');
    });
  }

  return Object.freeze({
    init,
  });
}

AddressForm().init();
