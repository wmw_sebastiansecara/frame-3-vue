/**
 * Template: Login
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Login template.
 *
 * @namespace login
 */
import cssClasses from '../../helpers/cssClasses';
import {on} from '../../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  customerLoginForm: '[js-password-form="customerLoginForm"]',
  cancelPasswordLink: '[js-password-form="cancelPasswordLink"]',
  recoverPasswordForm: '[js-password-form="recoverPasswordForm"]',
  resetPasswordSuccess: '[js-password-form="resetPasswordSuccess"]',
  forgotPasswordLink: '[js-password-form="forgotPasswordLink"]',
};

export function passwordForm() {

  /**
   * DOM Node selectors.
   */
  const nodeSelectors = {
    recoverPasswordForm: document.querySelector(selectors.recoverPasswordForm),
    customerLoginForm: document.querySelector(selectors.customerLoginForm),
    resetPasswordSuccess: document.querySelector(selectors.resetPasswordSuccess),
    cancelPasswordLink: document.querySelector(selectors.cancelPasswordLink),
    forgotPasswordLink: document.querySelector(selectors.forgotPasswordLink),
  };

  /**
   * Initialise login form.
   */
  function init() {
    setToggleEvents();

    if (nodeSelectors.forgotPasswordLink) {
      checkUrlHash();
      resetPasswordSuccess();
    }
  }

  /**
   * Set toggle events on forgot password and cancel links.
   */
  function setToggleEvents() {
    on('click', nodeSelectors.forgotPasswordLink, (event) => handlePasswordToggle(event));
    on('click', nodeSelectors.cancelPasswordLink, (event) => handlePasswordToggle(event));
  }

  /**
   * Handle login and forgot password form toggle.
   */
  function handlePasswordToggle(event) {
    event.preventDefault();
    toggleRecoverPasswordForm();
  }

  /**
   * Check if #recover is in url hash.
   * - This can occur when the user is coming from an external source.
   */
  function checkUrlHash() {
    if (window.location.hash === '#recover') {
      toggleRecoverPasswordForm();
    }
  }

  /**
   *  Show/Hide recover password form.
   */
  function toggleRecoverPasswordForm() {
    nodeSelectors.recoverPasswordForm.classList.toggle(cssClasses.hidden);
    nodeSelectors.customerLoginForm.classList.toggle(cssClasses.hidden);
  }

  /**
   *  Show reset password success message if posted successfully.
   */
  function resetPasswordSuccess() {
    if (!nodeSelectors.resetPasswordSuccess) {
      return;
    }
    nodeSelectors.resetPasswordSuccess.classList.remove(cssClasses.hidden);
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
}

passwordForm().init();
