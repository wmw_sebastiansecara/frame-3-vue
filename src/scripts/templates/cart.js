/**
 * Template: Cart
 * ------------------------------------------------------------------------------
 * Loosely coupled cart page scripts.
 *
 * @namespace cart
 */
import CartTable from '../components/cart-table';
import cssClasses from '../helpers/cssClasses';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-cart="container"]',
  table: '[js-cart-table="container"]',
  panel: '[js-cart="panel"]',
};

/**
 * DOM node selectors.
 */
const nodeSelectors = {
  panel: document.querySelector(selectors.panel),
};

/**
 * Global instance strings.
 */
const strings = {
  title: theme.strings.cart.general.title,
  empty: theme.strings.cart.general.empty,
  continueBrowsing: theme.strings.cart.general.continue_browsing,
};

document.addEventListener('DOMContentLoaded', () => {
  CartTable(selectors.table).init();

  Frame.EventBus.listen('AjaxCart:cartEmpty', () => {
    document.querySelector(selectors.container).innerHTML = `
      <div class="container">
        <div class="row">
          <div class="col xs12 m6 offset-m3 text-center">
            <h1>${strings.title}</h1>
            <p>${strings.empty}</p>
            <a href="/collections/all" class="button button--primary button--large">${strings.continueBrowsing}</a>
          </div>
        </div>
      </div>
    `;
  });

  Frame.EventBus.listen('Cart:updatingPrices', () => {
    nodeSelectors.panel.classList.add(cssClasses.loading);
  });

  Frame.EventBus.listen('Cart:pricesUpdated', () => {
    nodeSelectors.panel.classList.remove(cssClasses.loading);
  });
});
