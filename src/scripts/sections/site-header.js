/**
 * Section: Site header
 * ------------------------------------------------------------------------------
 * Contains highly coupled scripts to control site header components
 *
 * @namespace siteHeader
 */
import Cookies from 'js-cookie';
import debounce from 'lodash-es/debounce';
import {register} from '@shopify/theme-sections';

import cssClasses from '../helpers/cssClasses';
import {on} from '../helpers/utils';
import Toggle from '../components/toggle';
import Modal from '../components/modal';
import Notification from '../components/notification';
import ScrollDirection from '../mixins/scroll-direction';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-site-header="container"]',
  megaNavToggle: '[js-site-header="megaNavToggle"]',
  siteNavItem: '[js-site-header="siteNavItem"]',
  siteNavLink: '[js-site-header="siteNavLink"]',
  navLink: '[js-site-header="navLink"]',
  storeIcon: '[js-site-header="storeIcon"]',
  redirectModalToggle: '[js-site-header="redirectModalToggle"]',
};

/**
 * Instance cookies.
 */
const cookieSelector = {
  geolocation: 'redirect_geolocation',
  settings: 'redirect_store_settings',
};

const hideMegaNavTimer = 500;
let blurTimer;

/**
 * Create a new site header object.
 */
register('site-header', {

  /**
   * Callback function when the section is loaded in via the 'sections.load()' function or by Theme Editor
   * 'shopify:section:unload' event
   */
  onLoad() {
    this.constructComponents();
    this.init();
  },

  /**
   * Component selectors.
   */
  setNodeSelectors() {
    this.container = document.querySelector(selectors.container);
    this.megaNavToggle = [...document.querySelectorAll(selectors.megaNavToggle)];
    this.navLink = [...document.querySelectorAll(selectors.navLink)];
    this.storeIcon = document.querySelector(selectors.storeIcon);
    this.redirectModalToggle = document.querySelector(selectors.redirectModalToggle);
  },

  /**
   * Component variables.
   */
  setVariables() {
    this.blurTimer = blurTimer;
    this.hideMegaNavTimer = hideMegaNavTimer;
    this.ariaExpandedString = 'aria-expanded';
  },

  /**
   * Instantiate search bar toggle.
   */
  constructSearchToggle() {
    this.SearchBar = Toggle({
      toggleSelector: 'searchBarHeader',
    });
  },

  /**
   * Instantiate announcement component.
   */
  constructAnnouncement() {
    this.Announcement = Notification({
      container: '.site-header__announcement',
      enableCookies: true,
      cookie: {
        namespace: 'frame_announcement',
        value: 'dismissed',
      },
    });
  },

  /**
   * Instantiate a collapsed header on scroll using scrollDirection helper.
   */
  constructStickyHeader() {
    this.AnimatedHeader = ScrollDirection({
      throttle: 250,
      start: 100,
    });
  },

  /**
   * Construct section components.
   */
  constructComponents() {
    this.constructSearchToggle();
    this.constructAnnouncement();
    this.constructStickyHeader();
  },

  /**
   * Initialise component.
   */
  init() {
    this.setNodeSelectors();
    this.setScrollEvents();
    this.setStickyHeader();
    this.setSearchToggle();

    if (theme.features.redirectModal) {
      this.setRedirectModalToggle();
      this.setStoreIcon();
    }
  },

  /**
   * Set redirect modal toggle events.
   */
  setRedirectModalToggle() {
    const modal = new Modal({
      id: 'redirect-modal',
    });

    on('click', this.redirectModalToggle, (event) => {
      event.preventDefault();
      modal.show('redirect-modal');
    });
  },

  /**
   * Set search toggle events.
   */
  setSearchToggle() {
    this.SearchBar.init();
  },

  /**
   * Set sticky header events after document has loaded styling.
   */
  setStickyHeader() {
    on('load', () => {
      this.AnimatedHeader.init();
      this.container.parentNode.classList.add(cssClasses.sticky);
      this.updateBodyOffset();
      on('resize', debounce(() => this.handleResizeEvents(), 250));
    });
  },

  /**
   * Get store icon using cookies and other stored information.
   */
  getStoreIcon() {
    const storeSettings = Cookies.getJSON(cookieSelector.settings);
    const storeCode = (typeof storeSettings === 'undefined' ? theme.store.storeCode : storeSettings.loc);
    return storeCode.toLowerCase();
  },

  /**
   * Update the icon based on the user's store settings.
   * @
   */
  setStoreIcon(countryCode = this.getStoreIcon()) {
    this.storeIcon.className = `flag-icon flag-icon-${countryCode}`;
  },

  /**
   * Handle resize events by requesting animation frame.
   */
  handleResizeEvents() {
    this.updateBodyOffset();
  },

  /**
   * Re-paint body padding-top using current header height.
   */
  updateBodyOffset() {
    document.body.style.paddingTop = `${this.container.parentNode.offsetHeight}px`;
  },

  /**
   * Cancels the blur timer if the focus is still on a Nav Link item
   * @param {HTMLElement} element - The Mega Nav link element
   */
  setKeyFocus(element) {
    on('focus', element, () => window.clearTimeout(this.blurTimer));
  },

  /**
   * Sets a timer that hides the Mega Nav if no Site Nav element is focused
   * @param {HTMLElement} element - The Mega Nav link element
   */
  setKeyBlur(element) {
    const siteNavParent = this.getSiteNavParent(element);
    on('blur', element, () => {
      this.blurTimer = window.setTimeout(() => this.hideMegaNav(siteNavParent), hideMegaNavTimer);
    });
  },

  /**
   * Hides active sibling Mega Nav items and sets aria-expanded to false using hideMegaNav function
   * @param {HTMLElement} element - The Mega Nav toggle button
   */
  hideSiblingMegaNavs(element) {
    const siteHeader = element.parentNode;
    siteHeader.childNodes.forEach((child) => {
      if (child !== element && child.nodeType === 1 && child.classList.contains(cssClasses.active)) {
        this.hideMegaNav(child);
      }
      return element;
    });
  },

  /**
   * Displays a Mega Nav item and sets aria-expanded to true
   * @param {HTMLElement} siteNavParent - The Mega Nav
   */
  showMegaNav(siteNavParent) {
    siteNavParent.classList.add(cssClasses.active);
    siteNavParent.querySelector(selectors.siteNavLink).setAttribute(this.ariaExpandedString, true);
    siteNavParent.querySelector(selectors.megaNavToggle).setAttribute(this.ariaExpandedString, true);
  },

  /**
   * Hides a Mega Nav item and sets aria-expanded to false
   * @param {HTMLElement} siteNavParent - The Mega Nav
   */
  hideMegaNav(siteNavParent) {
    siteNavParent.classList.remove(cssClasses.active);
    siteNavParent.querySelector(selectors.siteNavLink).setAttribute(this.ariaExpandedString, false);
    siteNavParent.querySelector(selectors.megaNavToggle).setAttribute(this.ariaExpandedString, false);
  },

  /**
   * Selects the siteNav parent
   * @param {HTMLElement} element - The Nav List item or the megaNavToggle button
   */
  getSiteNavParent(element) {
    return element.closest(selectors.siteNavItem);
  },

  /**
   * Toggle Mega Nav, update aria tags and hide other Mega Nav items
   * @param {HTMLElement} element - The Nav List item or the Mega Nav Toggle button
   */
  toggleMegaNav(element) {
    const siteNavParent = this.getSiteNavParent(element);

    this.hideSiblingMegaNavs(siteNavParent);
    if (siteNavParent.classList.contains(cssClasses.active)) {
      this.hideMegaNav(siteNavParent);
      return element;
    }
    this.showMegaNav(siteNavParent);
    return element;
  },

  /**
   * Set listeners based on scroll direction.
   */
  setListeners() {
    Frame.EventBus.listen('Notification:dismissed', () => {
      this.updateBodyOffset();
    });

    Frame.EventBus.listen('ScrollDirection:down', () => {
      this.container.parentNode.classList.add(cssClasses.collapsed);
    });

    Frame.EventBus.listen('ScrollDirection:top', () => {
      this.container.parentNode.classList.remove(cssClasses.collapsed);
    });

    Frame.EventBus.listen('Toggle:searchBarHeader:open', () => {
      document.documentElement.classList.add('js-search-active');
    });

    Frame.EventBus.listen('Toggle:searchBarHeader:close', () => {
      document.documentElement.classList.remove('js-search-active');
    });

    Frame.EventBus.listen('Redirect:updateIcon', (countryCode) => {
      this.setStoreIcon(countryCode);
    });


    this.megaNavToggle.forEach((element) => {
      on('click', element, () => this.toggleMegaNav(element));
    });

    this.navLink.forEach((element) => {
      this.setKeyBlur(element);
      this.setKeyFocus(element);
    });
  },

  /**
   * Set scroll events.
   */
  setScrollEvents() {
    this.setListeners();
  },
});
