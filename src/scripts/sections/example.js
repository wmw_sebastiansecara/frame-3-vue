/**
 * Section: Example
 * ------------------------------------------------------------------------------
 * Example section configuration for complete theme support
 * - https://github.com/Shopify/theme-scripts/tree/master/packages/theme-sections
 *
 * @namespace example
 */
import {register} from '@shopify/theme-sections';
import cssClasses from '../helpers/cssClasses';

/**
 * DOM selectors.
 */
const selectors = {
  container: '.container',
};

/**
 * Register the `example-section` section type.
 */
register('example-section', {

  /**
   * Custom public section method.
   */
  publicMethod() {
    const container = document.querySelector(selectors.container);

    this.nodeSelectors = {
      container,
    };
  },

  /**
   * Callback function when section is loaded via 'sections.load()' or by the Theme Editor 'shopify:section:load' event.
   */
  onLoad() {
    // Do something when a section instance is loaded
    this.publicMethod();
    this.nodeSelectors.container.classList.add(cssClasses.active);
  },

  /**
   * Callback function when section unloaded by the Theme Editor 'shopify:section:unload' event.
   */
  onUnload() {
    // Do something when a section instance is unloaded
  },

  /**
   * Callback function when section is selected by the Theme Editor 'shopify:section:select' event.
   */
  onSelect() {
    // Do something when a section instance is selected
  },

  /**
   * Callback function when section is deselected by the Theme Editor 'shopify:section:deselect' event.
   */
  onDeselect() {
    // Do something when a section instance is selected
  },

  /**
   * Callback function when section block is selected by the Theme Editor 'shopify:block:select' event.
   */
  onBlockSelect() {
    // Do something when a section block is selected
  },

  /**
   * Callback function when section block is deselected by the Theme Editor 'shopify:block:deselect' event.
   */
  onBlockDeselect() {
    // Do something when a section block is deselected
  },
});
