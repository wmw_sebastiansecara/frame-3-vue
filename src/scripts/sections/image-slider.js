/**
 * Section: Image slider
 * ------------------------------------------------------------------------------
 * Large component displaying image galleries in a carousel.
 *
 * @namespace imageSlider
 */
import {register} from '@shopify/theme-sections';
import Carousel from '../components/carousel';

/**
 * DOM selectors.
 */
const selectors = {
  id: 'ImageSlider',
  container: '.image-slider',
};

/**
 * Instance globals.
 */
let imageSlider = {};

/**
 * Defaults.
 */
const config = {
  cellSelector: '.image-slider__cell',
  pageDots: true,
  prevNextButtons: false,
};

/**
 * Image slider section constructor.
 * Executes on page load as well as Theme Editor `section:load` events.
 * @param {string} container - Selector for the section container DOM element.
 */
register('image-slider', {

  /**
   * Construct section events
   */
  onLoad() {
    const namespace = `#${selectors.id}-${this.id}`;
    imageSlider = new Carousel(namespace, config);
    imageSlider.init();
  },

  /**
   * Event callback for Theme Editor `section:unload` event
   */
  onUnload() {
    imageSlider.reload();
  },

  /**
   * Event callback for Theme Editor `section:select` event
   */
  onSelect() {
    imageSlider.resize();
  },
});
