/**
 * Section: Recommended Products
 * ------------------------------------------------------------------------------
 * A section for fetching and displaying recommended products.
 * Uses the Shopify recommended products Ajax endpoint
 */
import {formatMoney} from '@shopify/theme-currency';
import {register} from '@shopify/theme-sections';
import axios from 'axios';
import debounce from 'lodash-es/debounce';

import cssClasses from '../helpers/cssClasses';
import breakpoints from '../helpers/breakpoints';
import {on} from '../helpers/utils';
import Carousel from '../components/carousel';
import QuickView from '../components/quick-view';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-recommended-products="container"]',
  carousel: '[js-recommended-products="carousel"]',
  slide: '[js-recommended-products="slide"]',
  productLink: '[js-product-link]',
  productTitle: '[js-product-link="product-title"]',
  productPrice: '[js-product-price]',
  responsiveImage: '[js-responsive-image]',
};

/**
 * Instance globals.
 */
let productCardCarousel = {};

/**
 * Register the `recommended-products` section type.
 */
register('recommended-products', {

  /**
   * Initialise section.
   */
  init() {
    this.cacheSelectors();
  },

  /**
   * Cache selectors.
   * - Define necessary variables and make available for component use.
   */
  cacheSelectors() {
    const container = document.querySelector(selectors.container);
    const section = document.querySelector(`[data-section-id="${this.id}"]`);
    const carousel = document.querySelector(`[data-section-id="${this.id}"] ${selectors.carousel}`);
    const slide = document.querySelector(`[data-section-id="${this.id}"] ${selectors.slide}`);

    this.cache = {
      section,
      container,
      carousel,
      slide,
    };
  },

  /**
   * Set window resize events for responsive initialisation.
   */
  setResizeEvents() {
    on('resize', debounce(this.handleResizeEvent, 250));
  },

  /**
   * Fetch recommended products data from Shopify Recommended Products API
   * @param {number} productId - Id of product to base recommended products on
   * @param {number} limit - Limit for number of products returned from API
   */
  fetchRecommendedProducts(productId, limit) {
    const productLimit = this.getProductLimit() ? this.getProductLimit() : limit;

    axios.get(`/recommendations/products.json?product_id=${productId}&limit=${productLimit}`)
      .then((response) => {
        const products = response.data.products;

        if (products && products.length > 0) {
          this.renderRecommendedProducts(products);
          this.setResizeEvents();
          return response;
        }
        throw new Error('No recommended products to display');
      })
      .catch((error) => {
        this.container.remove();
        window.console.error(error);
      });
  },

  /**
   * Render product cards from returned recommendation data
   * - Creates a DocumentFragment to store generated HTML.
   * - productCardTemplate node is duplicated as template and product data added.
   * - fragment appended to templateContainer to add in DOM.
   * - Class added to templateContainer to show in DOM once elements appended.
   * - Carousel initialized if products length > 4.
   * @param {Object} products - Recommended products api response products array.
   */
  renderRecommendedProducts(products) {
    const productCardTemplate = this.cache.slide;
    const templateContainer = this.cache.carousel;

    const fragment = document.createDocumentFragment();

    products.forEach((product) => {
      const template = productCardTemplate.cloneNode(true);
      const renderObjects = {product, template};

      this.updateProductObject(renderObjects);
      fragment.appendChild(template);
    });

    productCardTemplate.parentNode.removeChild(productCardTemplate);
    templateContainer.appendChild(fragment);
    this.cache.section.classList.add(cssClasses.added);
    this.constructCarousel();
    QuickView().init();
  },

  /**
   * Handle resize events through debounce callback.
   * - Disable zoom on small devices.
   */
  handleResizeEvent() {
    if (!window.matchMedia(`(min-width: ${breakpoints.large})`).matches) {
      productCardCarousel.init();
      return;
    }

    if (typeof productCardCarousel.destroy() === 'function') {
      productCardCarousel.destroy();
    }
  },

  /**
   * Update product pages and links.
   * @param {Object} renderObjects - Object containing product data and template nodeList.
   */
  updateProductObject(renderObjects) {
    this.createProductBadges(renderObjects);
    this.updateProductImages(renderObjects);
    this.updateProductLinks(renderObjects);
    this.updateProductPrice(renderObjects);
    this.updateProductTitle(renderObjects);
  },

  /**
   * Create product badge HTML
   * - Loops through product tags and filters out tags containing 'badge:'.
   * - For all tags containing 'badge:' runs renderBadgeTemplate.
   * @param {Object} renderObjects - Object containing product data and template nodeList.
   * @param {Object} renderObjects.product - Product data returned from Recommended Products API.
   * @param {nodeList} renderObjects.template - DOM Node of Product Card where product data is being added.
   */
  createProductBadges(renderObjects) {
    const badgeNode = document.createElement('div');
    badgeNode.classList.add('card__badge-listing');
    badgeNode.innerHTML = renderObjects.product.tags.filter((tag) => tag.indexOf('badge:') > -1)
      .map((tag) => this.renderBadgeTemplate(tag))
      .join('');
    if (badgeNode.innerHTML) {
      renderObjects.template.insertBefore(badgeNode, renderObjects.template.firstChild);
    }
  },

  /**
   * Return a div containing tag data
   * @param {String} tag - Product tag.
   */
  renderBadgeTemplate(tag) {
    return `
      <div class="card__badge badge badge--secondary">
        <span>${tag.replace('badge:', '')}</span>
      </div>
    `;
  },

  /**
   * Update product links in HTML template
   * - Sets selectors.productLink items to renderObjects.product.url
   * - Updates Quick view button data-product-url to product.handle
   * @param {Object} renderObjects - Object containing product object and template nodeList.
   * @param {Object} renderObjects.product - Product data returned from Recommended Products API
   * @param {nodeList} renderObjects.template - DOM Node of Product Card where product data is being added
   */
  updateProductLinks(renderObjects) {
    const links = renderObjects.template.querySelectorAll(selectors.productLink);
    const quickViewButton = renderObjects.template.querySelector('[js-quick-view="toggle"]');

    [...links].forEach((link) => {
      link.href = renderObjects.product.url;
    });

    if (typeof quickViewButton !== 'undefined') {
      quickViewButton.dataset.productUrl = `/products/${renderObjects.product.handle}`;
    }
  },

  /**
   * Update product title in HTML template
   * - Sets selectors.productTitle items to renderObjects.product.title.
   * @param {Object} renderObjects - Object containing product object and template nodeList.
   * @param {Object} renderObjects.product - Product data returned from Recommended Products API.
   * @param {nodeList} renderObjects.template - DOM Node of Product Card where product data is being added.
   */
  updateProductTitle(renderObjects) {
    renderObjects.template.querySelector(selectors.productTitle).innerText = renderObjects.product.title;
  },

  /**
   * Update product images in HTML template
   * - Sets correct lazyload classes on the image elements.
   * - Set first image element to be featured_image if property exists.
   * - Loops through remaining product.image and imageElements to show additional images.
   * - Runs generateImageUrl to generate appropriate image urls.
   * @param {Object} renderObjects - Object containing product object and template nodeList.
   * @param {Object} renderObjects.product - Product data returned from Recommended Products API.
   * @param {nodeList} renderObjects.template - DOM Node of Product Card where product data is being added.
   */
  updateProductImages(renderObjects) {
    const imageElements = renderObjects.template.querySelectorAll(`${selectors.responsiveImage} img`);
    [...imageElements].forEach((el, index) => {
      let image = renderObjects.product.images[0];

      el.classList.remove(cssClasses.lazyloaded);
      el.classList.add(cssClasses.lazyload);

      if (index === 0 && renderObjects.product.featured_image) {
        image = renderObjects.product.featured_image;
      } else if (renderObjects.product.images[index]) {
        image = renderObjects.product.images[index];
      }

      el.src = this.generateImageUrl(image, '3x');
      el.dataset.src = this.generateImageUrl(image, 'responsive');
    });
  },

  /**
   * Modifies image URL string to add size parameter or LazySizes responsive markup
   * @param {url} image - Product Image URL.
   * @param {string} size - Either "responsive" or size string.
   */
  generateImageUrl(image, size) {
    const imageSize = size === 'responsive' ? '{width}x' : size;
    return image.replace('.jpg', `_${imageSize}.jpg`);
  },

  /**
   * Get slider limit value
   */
  getProductLimit() {
    return this.cache.carousel.dataset.recommendedLimit;
  },

   /**
   * Update product price in HTML template
   * - Finds selectors.productPrice elements in template node
   * - Renders new product price into elements using renderProductPrice
   * @param {Object} renderObjects - Object containing product object and template nodeList to get price from.
   * @param {object} renderObjects.product - Product data returned from Recommended Products API
   * @param {nodeList} renderObjects.template - DOM Node of Product Card where product data is being added
   */
  updateProductPrice(renderObjects) {
    renderObjects.template.querySelector(selectors.productPrice).innerHTML = this.renderProductPrice(renderObjects.product);
  },

  /**
   * Get variant price template.
   * - Checks if it's discounted or not.
   * @param {Object} product - Product object to get price from.
   */
  renderProductPrice(product) {
    if (product.price_varies) {
      if (product.compare_at_price > product.price) {
        return `
          ${theme.strings.productPrice.onSaleFrom}
          <span js-currency="price" data-price="${product.price}">${formatMoney(product.price, theme.moneyFormat)}</span>
          <span class="visually-hidden">${theme.strings.productPrice.regularPrice}</span>
          <s js-currency="price" data-price="${product.compare_at_price}">${formatMoney(product.compare_at_price, theme.moneyFormat)}</s>
        `;
      }
      return `
      ${theme.strings.productPrice.fromText}
      <span js-currency="price" data-price="${product.price}">${formatMoney(product.price, theme.moneyFormat)}</span>
      `;
    }
    return `<span js-currency="price" data-price="${product.price}">${formatMoney(product.price, theme.moneyFormat)}</span>`;
  },

  /**
   * Get total number of product cards.
   */
  getProductCardCount() {
    return document.querySelectorAll(selectors.slide).length;
  },

  /**
   * Construct Carousel
   */
  constructCarousel() {
    productCardCarousel = Carousel(selectors.carousel, {
      cellSelector: selectors.slide,
      cellAlign: 'center',
      prevNextButtons: true,
    });

    /**
     * Initiate carousel on large screens if there are more than 3 items.
     */
    if (
      this.getProductCardCount() > 3 &&
      !window.matchMedia(`(min-width: ${breakpoints.large})`).matches
    ) {
      productCardCarousel.init();
    }
  },

  /**
   * Callback function when section is loaded via 'sections.load()' or by the Theme Editor 'shopify:section:load' event.
   */
  onLoad() {
    this.init();
    this.fetchRecommendedProducts(this.cache.section.dataset.productId);
  },
});
