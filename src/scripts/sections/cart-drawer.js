/**
 * Section: Cart drawer
 * -----------------------------------------------------------------------------
 * Contains highly coupled scripts to render cart items onto a drawer.
 *
 * @namespace cartDrawer
 */
import axios from 'axios';
import {register} from '@shopify/theme-sections';
import {formatMoney} from '@shopify/theme-currency';
import {getSizedImageUrl} from '@shopify/theme-images';
import * as cart from '@shopify/theme-cart';
import Cookies from 'js-cookie';

import cssClasses from '../helpers/cssClasses';
import Toggle from '../components/toggle';
import {on} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  body: '[js-cart-drawer="body"]',
  container: '[js-cart-drawer="container"]',
  footer: '[js-cart-drawer="footer"]',
  subtotal: '[js-ajax-cart="subtotal"]',
  lineItem: '[js-ajax-cart="lineItem"]',
};

/**
 * Attribute and data selectors.
 */
const dataSelectors = {
  ajaxCart: 'js-ajax-cart',
};

/**
 * Create a new drawer instance.
 */
const Drawer = Toggle({
  scrollLock: true,
  toggleSelector: 'cartDrawer',
});

/**
 * Register the `cart-drawer` section type.
 */
register('cart-drawer', {

  /**
   * Initialise section.
   */
  init() {
    Drawer.init();
    this.setNodeSelectors();
    this.setRemoveEvents(this.container);
  },

  /**
   * Set cached node selectors.
   */
  setNodeSelectors() {
    this.body = document.querySelector(selectors.body);
    this.container = document.querySelector(selectors.container);
    this.footer = document.querySelector(selectors.footer);
    this.subtotal = document.querySelector(selectors.subtotal);
  },

  /**
   * Set listeners for each instance.
   */
  setListeners() {
    Frame.EventBus.listen('AjaxCart:itemAdded', () => this.open());
    Frame.EventBus.listen(['AjaxCart:cartCleared', 'AjaxCart:itemRemoved'], () => this.render());
    Frame.EventBus.listen(['AjaxCart:removingItem'], (item) => this.setLoadingState(item));
  },

  /**
   * Render line items template.
   */
  render() {
    axios.get('/cart.js')
      .then((response) => {
        if (response.data.item_count > 0) {
          this.setActiveState(response.data);

          if (theme.features.currencyConverter) {
            Frame.MultiCurrency.CurrencyConverter.convertAll(Cookies.get('new_currency'));
          }
          return;
        }

        this.setEmptyState();
        throw new Error('Cart is empty');
      })
      .catch((error) => {
        window.console.error(error);
        return error;
      });
  },

  /**
  * Set active state.
  * @param {object} data - Cart data.
  */
  setActiveState(data) {
    this.body.innerHTML = this.getActiveTemplate(data.items);
    this.subtotal.setAttribute('data-price', data.total_price);
    this.subtotal.innerHTML = formatMoney(data.total_price, theme.moneyFormat);
    this.footer.classList.remove(cssClasses.hidden);

    window.setTimeout(() => {
      this.container.classList.remove(cssClasses.loading);
    }, 500);
  },

  /**
   * Set event delegation to remove items on container.
   * @param {HTMLElement} container - The container to apply the remove events on.
   */
  setRemoveEvents(container) {
    on('click', container, (event) => this.handleRemoveEvent(event));
  },

  /**
   * Handle remove events.
   * @param {Event} event - Click event.
   */
  handleRemoveEvent(event) {
    if (!this.isTargetRemove(event.target)) {
      return;
    }

    event.preventDefault();
    Frame.EventBus.emit('AjaxCart:removingItem', event.target.closest(selectors.lineItem));

    cart.removeItem(event.target.dataset.key)
      .then((response) => {
        Frame.EventBus.emit('AjaxCart:itemRemoved', response);
        return response;
      })
      .catch((error) => {
        window.console.error(error);
        return error;
      });
  },

  /**
   * Detect remove target click.
   * @param {HTMLElement} target - Clicked target to check remove attributes against.
   */
  isTargetRemove(target) {
    return (
      typeof target.attributes[dataSelectors.ajaxCart] !== 'undefined' &&
      target.attributes[dataSelectors.ajaxCart].nodeValue === 'remove'
    );
  },

  /**
   * Set loading state.
   * @param {object} element - DOM line item.
   */
  setLoadingState(element) {
    element.classList.add(cssClasses.removing);
  },

  /**
  * Set empty state.
  */
  setEmptyState() {
    this.body.innerHTML = this.getEmptyTemplate();
    this.footer.classList.add(cssClasses.hidden);
  },

  /**
  * Get cart drawer body template.
  * @param {Object} items - Cart line item object data.
  * @returns {string} - Template string.
  */
  getActiveTemplate(items) {
    return items.map((item) => this.getLineItemTemplate(item)).join('');
  },

  /**
   * Get empty cart template.
   */
  getEmptyTemplate() {
    return `<p>${theme.strings.cart.general.empty}</p>`;
  },

  /**
   * Get line item template.
   * @param {Object} item - Cart line item object data.
   * @returns {string} - Template.
   */
  getLineItemTemplate(item) {
    return `
      <div class="ajax-cart__line-item" js-ajax-cart="lineItem">
        <div class="ajax-cart__image responsive-image__wrapper">
          <img
            src="${getSizedImageUrl(item.image, '240x')}"
            srcset="
              ${getSizedImageUrl(item.image, '120x')} 300w,
              ${getSizedImageUrl(item.image, '180x')} 700w,
              ${getSizedImageUrl(item.image, '240x')} 1000w"
            alt="${item.title}"
          >
        </div>

        <div class="ajax-cart__description">
          <div class="row no-margin">
            <div class="col xs7 m7">
              ${this.isVendorEnabled() ? this.getVendorTitle(item) : ''}
              <a class="ajax-cart__title" href="${item.url}">${item.product_title}</a>
              ${this.getVariantTitle(item)}

              <p><button class="ajax-cart__remove" js-ajax-cart="remove" data-key="${item.key}" tabindex="0">Remove</button></p>
            </div>

            <div class="col xs5 m5">
              <div class="ajax-cart__price">
                ${item.quantity} × ${this.getLineItemPrice(item)}
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  },

  /**
   * Get variant price template.
   * - Checks if it's discounted or not.
   * @param {Object} item - Cart line item to get price from.
   */
  getLineItemPrice(item) {
    if (item.original_line_price !== item.line_price) {
      return `
        <span class="visually-hidden">${item.discounted_price}</span>
        <span js-currency="price" data-price="${item.price}">
          ${formatMoney(item.price, theme.moneyFormat)}
        </span>
        <span class="visually-hidden">${item.original_price}</span>
        <s>${formatMoney(item.original_price, theme.moneyFormat)}</s>
      `;
    }

    return `
      <span js-currency="price" data-price="${item.price}">
        ${formatMoney(item.price, theme.moneyFormat)}
      </span>
    `;
  },

  /**
   * Get variant titles.
   * @param {Object} items - Cart line item object data.
   * @returns {string}
   */
  getVariantTitle(item) {
    return (this.isVariantTitle(item)) ? `<p class="ajax-cart__variant-title caption">${item.variant_title}</p>` : '';
  },

  /**
   * Check if vendor title setting is enabled.
   */
  isVendorEnabled() {
    return JSON.parse(this.container.dataset.enableVendorTitle);
  },

  /**
   * Get vendor title template.
   * @param {object} item - Cart item to get vendor title from.
   */
  getVendorTitle(item) {
    return `<p class="ajax-cart__vendor caption">${item.vendor}</p>`;
  },

  /**
   * Check if variant title exists.
   * @param {object} item - Cart line item object data.
   */
  isVariantTitle(item) {
    return item.variant_options.length && !item.variant_options[0].includes('Default Title');
  },

  /**
   * Set the rednering state of the drawer.
   */
  setRenderingState() {
    this.container.classList.add(cssClasses.loading);
  },

  /**
   * Open drawer and force render.
   */
  open() {
    Drawer.open(this.container);
    this.setRenderingState();
    this.render();
  },

  /**
   * Close drawer.
   */
  close() {
    Drawer.close(this.container);
  },

  /**
   * Callback function when section is selected by the Theme Editor 'shopify:section:select' event.
   */
  onSelect() {
    Drawer.open(this.container);
  },

  /**
   * Callback function when section is deselected by the Theme Editor 'shopify:section:deselect' event.
   */
  onDeselect() {
    Drawer.close(this.container);
  },

  /**
   * Callback function when section is loaded via 'sections.load()' or by the Theme Editor 'shopify:section:load' event.
   */
  onLoad() {
    this.init();
    this.setListeners();
  },
});
