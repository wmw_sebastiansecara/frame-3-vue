/**
 * Section: Menu drawer
 * -----------------------------------------------------------------------------
 * Scripts to initiate menu drawer toggle and also display on admin
 *
 * @namespace menuDrawer
 */
import {register} from '@shopify/theme-sections';
import {disableBodyScroll, enableBodyScroll} from 'body-scroll-lock';

import cssClasses from '../helpers/cssClasses';
import {on, getSiblings} from '../helpers/utils';
import Toggle from '../components/toggle';

/**
 * DOM selectors.
 */
const selectors = {
  childMenuToggle: '[js-menu-drawer="childMenuToggle"]',
  grandChildMenuToggle: '[js-menu-drawer="grandChildMenuToggle"]',
};

/**
 * Instantiate menu drawer toggle.
 */
const MenuDrawer = Toggle({
  toggleSelector: 'menuDrawerToggle',
  scrollLock: true,
});

/**
 * Component selectors.
 */
const menus = [
  ...document.querySelectorAll(selectors.childMenuToggle),
  ...document.querySelectorAll(selectors.grandChildMenuToggle),
];

register('menu-drawer', {

  /**
   * Initiate toggle events on all sub-menus.
   */
  constructAccordionMenus() {
    [...menus].forEach((element) => {
      on('click', element, (event) => this.onItemClick(event, element));
    });
  },

  /**
   * Handle toggle event on menu item click.
   * @param {Event} event - Click event.
   * @param {HTMLElement} element - The menu item.
   */
  onItemClick(event, element) {
    event.preventDefault();
    this.toggleElement(element);
    this.toggleElement(getSiblings(element)[0]);
  },

  /**
   * Set listeners for each instance.
   */
  setListeners() {
    Frame.EventBus.listen(['Toggle:menuDrawerTarget:open'], () => disableBodyScroll(this.container));
    Frame.EventBus.listen(['Toggle:menuDrawerTarget:close'], () => enableBodyScroll(this.container));
  },

  /**
   * Check if item is active.
   * @param {HTMLElement} element - The menu item.
   */
  isItemActive(element) {
    return element.classList.contains(cssClasses.active);
  },

  /**
   * Toggle button state.
   * @param {HTMLElement} element - The toggled button.
   */
  toggleElement(element) {
    if (this.isItemActive(element)) {
      this.showMenu(element);
      return;
    }
    this.hideMenu(element);
  },

  /**
   * Show menu.
   * @param {HTMLElement} element - The item to show.
   */
  showMenu(element) {
    element.classList.remove(cssClasses.active);
    Frame.EventBus.emit('Toggle:menuDrawerTarget:open');
  },

  /**
   * Hide menu.
   * @param {HTMLElement} element - The item to hide.
   */
  hideMenu(element) {
    element.classList.add(cssClasses.active);
    Frame.EventBus.listen('Toggle:menuDrawerTarget:close');
  },

  /**
   * Initialise section.
   */
  init() {
    MenuDrawer.init();
    this.constructAccordionMenus();
  },

  /**
   * Event callback for Theme Editor `section:select` event
   */
  onSelect() {
    MenuDrawer.open();
  },

  /**
   * Event callback for Theme Editor 'shopify:section:deselect' event.
   */
  onDeselect() {
    MenuDrawer.close();
  },

  /**
   * Event callback for Theme Editor `section:load` event
   */
  onLoad() {
    this.init();
    this.setListeners();
  },
});
