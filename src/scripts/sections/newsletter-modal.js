/**
 * Section: Newsletter Modal
 * ------------------------------------------------------------------------------
 * Newsletter modal configuration for complete theme support
 * - https://github.com/Shopify/theme-scripts/tree/master/packages/theme-sections
 *
 * @namespace newsletterModal
 */
import Cookies from 'js-cookie';
import {register} from '@shopify/theme-sections';

import Modal from '../components/modal';
import cssClasses from '../helpers/cssClasses';

/**
 * Register the `newsletter-modal` section type.
 * Executes on page load as well as Theme Editor `section:load` events.
 * @param {string} container - selector for the section container DOM element
 */
register('newsletter-modal', {

  /**
   * Initialise section objects.
   */
  init() {
    this.settings = {
      id: 'newsletter-modal',
      namespace: 'newsletter_modal',
    };

    this.eventSelectors = {
      modalClose: `Modal:Close:${this.settings.id}`,
    };

    this.modal = new Modal({
      id: this.settings.id,
    });
  },

  /**
   * Set event listeners on component.
   */
  setListeners() {
    Frame.EventBus.listen(this.eventSelectors.modalClose, () => this.handleCloseEvents());
  },

  /**
   * Handle close callback function by setting cookie.
   */
  handleCloseEvents() {
    Cookies.set(this.settings.namespace, 'dismissed', {expires: 365});
  },

  /**
   * Check if a URL parameter exists to hide the notification modal.
   * @returns {Boolean} whether namespace parameter is in URL.
   */
  hasUrlParameter() {
    if (window.location.search.includes(`modal-hide=${this.settings.namespace}`)) {
      this.handleCloseEvents();
      return true;
    }

    return false;
  },

  /**
   * Get all modal elements currently on the DOM.
   * @returns {Array} All currently active modals.
   */
  getAllActiveModals() {
    return Array.from(document.querySelectorAll(`.modal.${cssClasses.active}`));
  },

  /**
   * Callback function when section is loaded in via the 'sections.load()' function or by Theme Editor
   * 'shopify:section:unload' event
   */
  onLoad() {
    this.init();
    this.setListeners();
    this.modal.init();

    /**
     * Set conditions on when to show the modal.
     */
    if (!Cookies.get(this.settings.namespace) && !this.hasUrlParameter()) {
      window.setTimeout(() => {
        if (this.getAllActiveModals().length !== 0) {
          return;
        }

        this.modal.show(this.settings.id);
      }, 10000);
    }
  },

  /**
   * Callback function when section is unloaded in the by Theme Editor 'shopify:section:unload' event.
   */
  onUnload() {
    this.modal.close(this.settings.id);
  },

  /**
   * Callback function when section is selected in the by Theme Editor 'shopify:section:select' event.
   */
  onSelect() {
    this.modal.show(this.settings.id);
  },

  /**
   * Callback function when section is deselected in the by Theme Editor 'shopify:section:deselect' event.
   */
  onDeselect() {
    this.modal.close();
  },
});
