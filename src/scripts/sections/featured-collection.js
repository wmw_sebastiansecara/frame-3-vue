/**
 * Section: Featured collection
 * ------------------------------------------------------------------------------
 * Configured section to display a single collection in product cards
 * - includes enable slider option
 *
 * @namespace featuredCollection
 */
import {register} from '@shopify/theme-sections';

import Carousel from '../components/carousel';
import QuickView from '../components/quick-view';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-featured-collection="carousel"]',
  cell: '[js-featured-collection="slide"]',
};

/**
 * Register a single featured collection section.
 * Executes on page load as well as Theme Editor `section:load` events.
 * @param {string} container - Selector for the section container DOM element.
 */
register('featured-collection', {

  /**
   * Cache selectors.
   * - Define necessary variables and make available for component use.
   */
  cacheSelectors() {
    const container = `[data-section-id="${this.id}"] ${selectors.container}`;
    const cells = `[data-section-id="${this.id}"] ${selectors.cell}`;

    this.cache = {
      container,
      cells,
    };
  },

  /**
   * Check if the section has enabled the slider option.
   */
  isSliderEnabled() {
    return document.querySelector(this.cache.container) !== null;
  },

  /**
   * Construct carousel using imported Carousel component.
   */
  constructCarousel() {
    Carousel(this.cache.container, {
      cellSelector: this.cache.cells,
      cellAlign: 'center',
      prevNextButtons: true,
    }).init();
  },

  /**
   * Construct new carousel on `section:load`.
   */
  onLoad() {
    QuickView().init();
    this.cacheSelectors();

    if (this.isSliderEnabled()) {
      this.constructCarousel();
    }
  },
});
