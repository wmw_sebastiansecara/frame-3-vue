/**
 * Layout: Password
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the product template.
 *
 * @namespace product
 */
import '../../styles/theme.scss';
import Toggle from '../components/toggle';

import EventBus from '../helpers/event-bus';

window.Frame = window.Frame || {};
window.Frame.EventBus = EventBus();

document.addEventListener('DOMContentLoaded', () => {

  /**
   * Instantiate password form toggle.
   */
  const FormToggle = Toggle({
    toggleSelector: 'loginFormToggle',
    overlay: false,
  });

  FormToggle.init();
});
