/**
 * Utils: Filters
 * -----------------------------------------------------------------------------
 * Output strings and values into a format you require.
 * - Imported into global Vue.filters in styleguide.js.
 */
export function handleize(value) {
  return value.toLowerCase().replace(/\s+/g, '-');
}

export function hash(value) {
  return `#${value}`;
}

export function capitalize(value) {
  return value.charAt(0).toUpperCase() + value.slice(1);
}

export function unbem(value) {
  return value.replace('__', ' ').replace('--', ' ');
}
