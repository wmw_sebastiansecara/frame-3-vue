/**
 * Layout: Theme
 * -----------------------------------------------------------------------------
 * Entry file for all global theme scripts and registering sections and modules
 *
 */
import axios from 'axios';

import '../../styles/theme.scss';

import {load} from '@shopify/theme-sections';
import {focusHash, bindInPageLinks} from '@shopify/theme-a11y';

/**
 * Global theme helpers.
 */
import '../helpers/polyfill';
import '../helpers/lazysizes';
import AjaxCart from '../helpers/ajax-cart';
import EventBus from '../helpers/event-bus';
import Redirects from '../helpers/redirects';
import Currency from '../helpers/currency';
import {getBrowser} from '../helpers/utils';

/**
 * Global theme section imports.
 */
import '../sections/cart-drawer';
import '../sections/menu-drawer';
import '../sections/site-header';

/**
 * Global theme component imports.
 */
import StoreSelector from '../components/store-selector';
import CookieBanner from '../components/cookie-banner';
import FreeShippingNotification from '../components/free-shipping-notification';
import RedirectModal from '../components/redirect-modal';
import SearchBar from '../components/search-bar';

/**
* Global Frame utils
*/
window.Frame = window.Frame || {};
window.Frame.EventBus = EventBus();

document.addEventListener('DOMContentLoaded', () => {
  if (getBrowser() === 'Internet Explorer') {
    axios.defaults.headers.common = {
      // IE11 caching bug.
      Pragma: 'no-cache',
    };
  }

  load('*');
  AjaxCart().init();
  SearchBar().init();
  CookieBanner().init();

  if (theme.features.redirectModal) {
    Redirects().init();
    RedirectModal().init();
  }

  if (theme.features.storeSelector) {
    StoreSelector().init();
  }

  if (theme.features.currencyConverter) {
    window.Frame.MultiCurrency.CurrencyConverter = Currency();
    Frame.MultiCurrency.CurrencyConverter.init();
  }

  if (theme.features.freeShippingNotification) {
    FreeShippingNotification().init();
  }

  focusHash();
  bindInPageLinks();
});
