/**
 * Layout: Styleguide
 * -----------------------------------------------------------------------------
 * Core entry file to register and display elements on the styleguide layout
 * - This entire styleguide layout is built by Vue.js
 */
import Vue from 'vue';
import {load} from '@shopify/theme-sections';
import '../../styles/styleguide.scss';

/**
 * Global styleguide helpers.
 */
import * as filters from './styleguide/utils/filters';
import '../helpers/polyfill';
import '../helpers/lazysizes';
import Currency from '../helpers/currency';
import EventBus from '../helpers/event-bus';
import Redirects from '../helpers/redirects';

/**
 * Sections.
 */
import '../sections/cart-drawer';
import '../sections/featured-collection';
import '../sections/image-slider';

/**
 * Components.
 */
import Accordion from '../components/accordion';
import FreeShippingNotification from '../components/free-shipping-notification';
import ProductForm from '../components/product-form';
import ProductGallery from '../components/product-gallery';
import QuantitySelector from '../components/quantity-selector';
import RedirectModal from '../components/redirect-modal';
import SearchBar from '../components/search-bar';

/**
 * Vue Components
 */
import SideNav from './styleguide/components/SideNav.vue';
import ContentBlock from './styleguide/components/ContentBlock.vue';
import ContentExample from './styleguide/components/ContentExample.vue';
import ContentSection from './styleguide/components/ContentSection.vue';
import SpecGrid from './styleguide/components/SpecGrid.vue';
import SpecGridRow from './styleguide/components/SpecGridRow.vue';
import IconGrid from './styleguide/components/IconGrid.vue';

/**
* Global Frame utils
*/
window.Frame = window.Frame || {};
window.Frame.EventBus = EventBus();

/**
 * Register global utility filters.
 */
Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key]);
});

new Vue({
  el: '#app',

  components: {
    ContentBlock,
    ContentExample,
    ContentSection,
    IconGrid,
    SideNav,
    SpecGrid,
    SpecGridRow,
  },

  created() {
    window.addEventListener('keyup', (event) => {
      if (event.keyCode === 27) {
        this.$root.$emit('navigation:toggle');
      }
    });

    this.$root.$on('updateView', () => {
      window.dispatchEvent(new window.CustomEvent('resize'));
    });
  },

  mounted() {

    /**
     * Initialise sections.
     */
    window.setTimeout(() => load('*'), 500);

    /**
     * Initialise components.
     */
    Accordion('.accordion').init();

    if (theme.features.currencyConverter) {
      Frame.MultiCurrency.CurrencyConverter = Currency();
      Frame.MultiCurrency.CurrencyConverter.init();
    }

    if (theme.features.freeShippingNotification) {
      FreeShippingNotification().init();
    }

    ProductGallery().init();
    ProductForm({
      container: 'quickview',
      enableSwatches: true,
    }).init();
    QuantitySelector('QuantitySelector-8').init();
    SearchBar().init();

    Redirects().init();
    RedirectModal().init();
  },
});
