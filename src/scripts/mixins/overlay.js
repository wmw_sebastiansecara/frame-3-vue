/**
 * Mixin: Overlay
 * -----------------------------------------------------------------------------
 * A simple mixin to toggle window overlay with escape events
 *
 * @namespace Overlay
 */
import cssClasses from '../helpers/cssClasses';
import {on, extendDefaults} from '../helpers/utils';

/**
 * Export overlay module as default.
 */
export default (config) => {

  /**
   * Instance default settings.
   */
  const defaults = {
    namespace: 'overlay',
    container: 'window-overlay',
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * Construct window overlay element on DOM.
   */
  function constructOverlay() {
    const element = document.createElement('div');
    element.classList.add(settings.container);
    return element;
  }

  /**
   * Get window overlay element.
   */
  function getOverlay() {
    return document.querySelector(`.${settings.container}`);
  }

  /**
   * Open overlay and set esc events.
   */
  function open() {
    if (getOverlay()) {
      return;
    }

    render();
    setCloseEvents();
  }

  /**
   * Hide overlay and remove esc event.
   */
  function close() {
    if (!getOverlay()) {
      return;
    }

    remove();
  }

  /**
   * Render overlay after constructing DOM element.
   */
  function render() {
    const windowOverlay = constructOverlay();
    document.body.appendChild(windowOverlay);
    windowOverlay.id = `${settings.namespace}Overlay`;

    // Dirty way to avoid MutationObserver by using an instant timeout.
    window.setTimeout(() => windowOverlay.classList.add(cssClasses.active), 1);
    Frame.EventBus.emit(`Overlay:${settings.namespace}:open`);
  }

  /**
   * Remove overlay from DOM after transitioning out.
   */
  function remove() {
    on('transitionend', getOverlay(), () => {
      if (getOverlay()) {
        document.body.removeChild(getOverlay());
      }
    });

    getOverlay().classList.remove(cssClasses.active);
  }

  /**
   * Set close events.
   */
  function setCloseEvents() {
    on('click', getOverlay(), () => handleClickEvent());
  }

  /**
   * Handle click event on overlay.
   */
  function handleClickEvent() {
    Frame.EventBus.emit(`Overlay:${settings.namespace}:close`, {
      selector: settings.namespace,
      target: document.getElementById(settings.namespace),
    });

    close();
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    open,
    close,
  });
};
