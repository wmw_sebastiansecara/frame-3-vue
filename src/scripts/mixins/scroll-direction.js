/**
 * Mixin: Scroll direction
 * ------------------------------------------------------------------------------
 * An event-based mixin to fire events when scroll direction has changed
 *
 * @namespace scrollDirection
 */
import throttle from 'lodash-es/throttle';
import {extendDefaults, on} from '../helpers/utils';

/**
 * Create a new scroll direction helper object.
 */
export default (config) => {

  /**
   * Instance default settings.
   */
  const defaults = {
    threshold: 5,
    throttle: 250,
    start: 100,
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);
  let previousScrollTop = 0;
  let currentScrollDirection = '';
  let newScrollDirection = '';

  /**
   * Set scrolling event with throttle parameter to limit the amount
   * the callback can fire
   */
  function setScrollingEvent() {
    on('scroll', throttle(handleScrollEvent, settings.throttle));
  }

  /**
   * Handle scrolling events using scroll top position.
   */
  function handleScrollEvent() {
    const scrollPosition = window.pageYOffset;
    setScrollState(scrollPosition);
  }

  /**
   * Set new scrolling direction value and fire off event.
   * @param {Number} scrollPosition - The window offset/scroll position.
   */
  function setScrollState(scrollPosition) {
    const scrollState = detectScrollDirection(scrollPosition);

    if (typeof scrollState === 'undefined') { return; }

    if (scrollState !== currentScrollDirection) {
      currentScrollDirection = newScrollDirection;
      Frame.EventBus.emit('ScrollDirection:changed', newScrollDirection);
    }
  }

  /**
   * Get the new scroll direction string.
   * @param {Number} scrollPosition - The window offset/scroll position.
   * @returns {string} New scroll direction string (at-top, up, down).
   */
  function detectScrollDirection(scrollPosition) {
    if (Math.abs(previousScrollTop - scrollPosition) <= settings.threshold) {
      return newScrollDirection;
    }

    if (scrollPosition > previousScrollTop && scrollPosition > settings.start) {
      Frame.EventBus.emit('ScrollDirection:down', 'down');
      newScrollDirection = 'down';
    } else {
      Frame.EventBus.emit('ScrollDirection:up', 'up');
      newScrollDirection = 'up';
    }

    if (scrollPosition <= settings.start + 10) {
      Frame.EventBus.emit('ScrollDirection:top', 'at-top');
      newScrollDirection = 'at-top';
    }

    previousScrollTop = scrollPosition;

    return newScrollDirection;
  }

  /**
   * Pure function to get current scrolling direction value.
   */
  function getScrollDirection() {
    return currentScrollDirection;
  }

  /**
   * Public interface.
   */
  return Object.freeze({
    init: setScrollingEvent,
    get: getScrollDirection,
  });
};
