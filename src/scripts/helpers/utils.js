/**
 * Helper: Utils
 * ------------------------------------------------------------------------------
 * Frame utility functions.
 *
 * @namespace utils
 */

/**
 * Get unique objects from object literal.
 * @param {Object} object - Array/Object to remove duplicates from.
 */
export function unique(object) {
  return object.filter((value, index, self) => self.indexOf(value) === index);
}

/**
 * Get the difference of two arrays.
 * @param {Array} array - An array containing two arrays to diff.
 */
export function difference(array1, array2) {
  return array1.concat(array2).filter((value, index, self) => {
    return self.indexOf(value) === self.lastIndexOf(value);
  });
}

/**
 * Combine two objects using properties as the override.
 * @param {object} defaults - Defaults options defined in script.
 * @param {object} properties - Options defined by user.
 * @return {object} - Defaults modified options.
 */
export function extendDefaults(defaults, properties) {
  for (const property in properties) {
    if (properties != null && typeof properties !== 'undefined') {
      defaults[property] = properties[property];
    }
  }

  return defaults;
}

/**
  * Retrieves input data from a form.
  * @param {HTMLForm} form - HTML form elements.
  * @returns {Object} - Form data as an object literal.
  */
export function formToJSON(elements) {
  return [].reduce.call(elements, (data, element) => {
    if (element.name && element.value) {
      data[element.name] = element.value;
    }
    return data;
  }, {});
}

/**
 * Serialize all form data into a query string.
 * @param {Node} form - The form to serialize.
 * @return {String} - The serialized form data.
 */
export function serialize(form) {
  const serialized = [];

  /**
   * Loop through each field in the form.
   */
  for (let i = 0; i < form.elements.length; i++) {
    const field = form.elements[i];

    // Don't serialize fields without a name, submits, buttons, file and reset inputs, and disabled fields
    if (!field.name || field.disabled || field.type === 'file' || field.type === 'reset' || field.type === 'submit' || field.type === 'button') { continue; }

    /**
     * If a multi-select, get all selections.
     */
    if (field.type === 'select-multiple') {
      for (let counter = 0; counter < field.options.length; counter++) {
        if (!field.options[counter].selected) {
          continue;
        }

        serialized.push(`${encodeURIComponent(field.name)}=${encodeURIComponent(field.options[counter].value)}`);
      }
    } else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
      serialized.push(`${encodeURIComponent(field.name)}=${encodeURIComponent(field.value)}`);
    }
  }

  return serialized.join('&');
}

/**
 * Shortcut function to add an event listener.
 * (c) 2017 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param {String} event -The event type.
 * @param {Node} elem - The element to attach the event to (optional, defaults to window).
 * @param {Function} callback - The callback to run on the event.
 * @param {Boolean} capture - If true, forces bubbling on non-bubbling events.
 */
export function on(event, elem = window, callback, capture = false) {

  /**
   * If only a string is passed into the element parameter.
   */
  if (typeof elem === 'string') {
    document.querySelector(elem).addEventListener(event, callback, capture);
    return;
  }

  /**
   * If an element is not defined in parameters, then shift callback across.
   */
  if (typeof elem === 'function') {
    window.addEventListener(event, elem);
    return;
  }

  /**
   * Default listener.
   */
  elem.addEventListener(event, callback, capture);
}

/**
 * Get all siblings of an element.
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param {Node} elem - The element you want get the siblings of.
 * @return {Array} - The siblings.
 */
export function getSiblings(elem) {
  return Array.prototype.filter.call(elem.parentNode.children, (sibling) => {
    return sibling !== elem;
  });
}

/**
 * Render function.
 * @param {String} template - The template string to render.
 * @param {Node} node - The node to render into.
 */
export function render(template, node) {
  if (!node) {
    return;
  }

  node.innerHTML = template;
}

/**
 * Browser detect and return a string
 * - https://developer.mozilla.org/en-US/docs/Web/API/Window/navigator
 * @returns {String} browser string.
 */
export function getBrowser() {
  const userAgent = window.navigator.userAgent;

  // The order matters here, and this may report false positives for unlisted browsers.
  if (userAgent.indexOf('Firefox') > -1) {
    // 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'
    return 'Firefox';
  } else if (userAgent.indexOf('Opera') > -1) {
    return 'Opera';
  } else if (userAgent.indexOf('Trident') > -1) {
    // 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0; wbx 1.0.0; rv:11.0) like Gecko'
    return 'Internet Explorer';
  } else if (userAgent.indexOf('Edge') > -1) {
    // 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299'
    return 'Edge';
  } else if (userAgent.indexOf('Chrome') > -1) {
    // 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36'
    return 'Chrome';
  } else if (userAgent.indexOf('Safari') > -1) {
    // 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1 980x1306'
    return 'Safari';
  }

  return 'unknown';
}

/* Get outerHTML of elements, taking care
 * of SVG elements in IE as well.
 *
 * @param {Element} el
 * @return {String}
 */
export function getOuterHTML(el) {
  if (el.outerHTML) {
    return el.outerHTML;
  } else {
    const container = document.createElement('div');
    container.appendChild(el.cloneNode(true));
    return container.innerHTML;
  }
}

/**
 * Check if object is a HTMLElement.
 * @param {object} element - HTML element to check.
 */
export function isElement(element) {
  return element instanceof window.Element || element instanceof window.HTMLDocument;
}

/**
 * Get query parameters as an object.
 * @return {Object} - Query object.
 */
export function getQueryParamsObject() {
  const result = {};
  const parser = document.createElement('a');

  parser.href = window.location.href;

  const query = parser.search.substring(1);
  const vars = query.split('&');

  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].split('=');
    result[pair[0]] = decodeURIComponent(pair[1]);
  }

  return result;
}
