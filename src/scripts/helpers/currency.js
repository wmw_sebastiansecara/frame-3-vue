/**
 * Helper: Currency
 * ------------------------------------------------------------------------------
 * An event based currency module listening for currency event changes.
 *
 * @namespace currency
 */

import 'url-search-params-polyfill';

import axios from 'axios';
import Cookies from 'js-cookie';

import Redirects from '../helpers/redirects';

/**
 * DOM selectors.
 */
const selectors = {
  price: '[js-currency="price"]',
  currencySelector: '[js-currency-selector="selector"]',
};

/**
 * Cookie selectors.
 */
const cookieSelector = {
  settings: 'redirect_store_settings',
};

/**
 * Export new redirect helper function.
 */
export default () => {

  /**
   * Instance globals.
   */
  const searchParams = new URLSearchParams(window.location.search);

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    price: document.querySelectorAll(selectors.price),
    currencySelector: document.querySelectorAll(selectors.currencySelector),
  };

  /**
   * Initialise component.
   */
  function init() {
    setListeners();
    setDefaultCookies();
    setDefaultState();
  }

  /**
   * Set component listeners.
   */
  function setListeners() {
    if (!nodeSelectors.currencySelector) { return; }

    [...nodeSelectors.currencySelector].forEach((element) => {
      Frame.EventBus.listen(`${element.id}:changed`, (response) => onCurrencyChange(response));
    });
  }

  /**
   * Handle currency selector change events.
   * @param {Object} response - Currency selector change event payload data.
   */
  function onCurrencyChange(response) {
    convertAll(response.currency.code.new);
  }

    /**
   * Return Geolocation data of current user.
   * @param {payload} - default emitted data from dropdown selector.
   */
  function fetchCurrencyRates() {
    return new Promise((resolve, reject) => {

      /**
       * Resolve stored exchange rate in cookie if API has already been called
       */
      if (typeof Cookies.get('exchange_rates') !== 'undefined') {
        resolve(Cookies.getJSON('exchange_rates'));
        return;
      }

      /**
       * Resolve cache response if API has already been called.
       */
      if (typeof Frame.MultiCurrency.ExchangeRate !== 'undefined') {
        resolve(Frame.MultiCurrency.ExchangeRate);
        return;
      }

      const args = `base=${theme.currency}&symbols=${getSupportedCurrencies()}`;

      axios.get(`https://data.fixer.io/api/latest?access_key=f882abf82f6c7ba900371ed64de0d6db&${args}`)
        .then((response) => {
          Frame.MultiCurrency.ExchangeRate = response.data.rates;
          Cookies.set('exchange_rates', response.data.rates, {expires: 1});
          resolve(response.data.rates);
          return response.data.rates;
        })
        .catch((err) => reject(err));
    });
  }

  /**
   * Handle conversion based on selected option.
   * @param {object} currencyCode - currency code to convert to.
   */
  function convertAll(currencyCode) {
    fetchCurrencyRates()
      .then((response) => {
        renderPrices(response[currencyCode], currencyCode, getCurrencyLocale(currencyCode));
        updateCookies(currencyCode);
        Frame.EventBus.emit('Currency:updated', currencyCode);
        return response;
      })
      .catch(() => {
        if (!currencyCode) { throw new Error('Currency code is not defined.'); }
        window.console.error('Could not convert currency.');
      });
  }

  /**
   * Update currency cookies using new currency code.
   * @param {String} currencyCode - Currency code to update the cookies with.
   */
  function updateCookies(currencyCode) {
    Cookies.set('old_currency', Cookies.get('new_currency'), {expires: 7});
    Cookies.set('new_currency', currencyCode, {expires: 7});
    Cookies.set('currency_locale', getCurrencyLocale(currencyCode), {expires: 7});
  }

  /**
   * Get currency locale from currency code defined in currency theme settings.
   * @param {String} currencyCode - Currency code to find.
   * @returns {String}
   */
  function getCurrencyLocale(currencyCode) {
    const found = Frame.MultiCurrency.SupportedCurrencies.find((currency) => {
      return (currency.currencyCode === currencyCode);
    }).currencyLocale;

    if (found) { return found; }

    throw new Error('Could not find currency in the section settings.');
  }

  /**
   * Render new prices inside every price targeted with the currency attribute.
   * @param {Number} rate - Conversion rate to multiple on amount.
   * @param {String} currencyCode - Currency code to convert to.
   * @param {String} currencyLocale - The locale format to use (€134,32).
   */
  function renderPrices(rate, currencyCode, currencyLocale) {
    [...document.querySelectorAll(selectors.price)].forEach((element) => {
      const convertedPrice = convert(element.dataset.price, rate);
      element.innerHTML = formatMoney(convertedPrice, currencyCode, currencyLocale);
    });
  }

  /**
   * Get a list of supported currency codes on store.
   * @param {String} storeCode - Store code to get supported currencies from (optional).
   * @return {String}
   */
  function getSupportedCurrencies(storeCode) {
    if (typeof storeCode === 'undefined') {
      return Frame.MultiCurrency.SupportedCurrencies.map((currency) => currency.currencyCode).join(',');
    }

    const matchingStore = Redirects().getMatchingStore(storeCode);
    return matchingStore.supportedCurrencies.replace(/\s/g, '');
  }

  /**
   * Convert price from store price to selected currency.
   * @param {Number} amount - The amount to convert.
   * @param {Number} rate - The rate to convert with.
   */
  function convert(amount, rate) {
    return ((amount * rate) / 100);
  }

  /**
   * Correctly format pricing based the locale.
   * @param {Number} amount - The amount to format.
   * @param {String} currencyCode - The currency code to format to.
   * @param {String} currencyLocale - The locale format to use (€134,32).
   */
  function formatMoney(amount, currencyCode, currencyLocale) {
    const options = {
      currency: currencyCode,
      currencyDisplay: 'symbol',
      style: 'currency',
    };

    return new Intl.NumberFormat(currencyLocale, options).format(amount);
  }

  /**
   * Set default cookies on first initialisation on theme.
   */
  function setDefaultCookies() {
    if (typeof Cookies.get('default_currency') === 'undefined') {
      Cookies.set('default_currency', Cookies.get('cart_currency'), {expires: 7});
    }

    if (typeof Cookies.get('new_currency') === 'undefined') {
      Cookies.set('new_currency', Cookies.get('cart_currency'), {expires: 7});
    }
  }

  /**
   * Set default currency if cookies are set.
   */
  function setDefaultState() {
    if (getDefaultCurrencyCode()) {
      Cookies.set('old_currency', Cookies.get('new_currency'), {expires: 7});
      Cookies.set('new_currency', getDefaultCurrencyCode(), {expires: 7});
    }

    convertAll(Cookies.get('new_currency'));
  }

  /**
   * Get default currency code.
   * - Query parameters
   * - Store settings cookie
   * - Theme default
   * @return {String} 3 letter currency code in uppercase.
   */
  function getDefaultCurrencyCode() {
    const currencyParams = searchParams.get('cur');

    if (currencyParams && currencyParams !== Cookies.get('new_currency')) {
      return currencyParams.toUpperCase();
    }

    if (typeof Cookies.getJSON(cookieSelector.settings) !== 'undefined') {
      return Cookies.getJSON(cookieSelector.settings).cur.toUpperCase();
    }

    if (
      typeof Cookies.getJSON(cookieSelector.settings) !== 'undefined' &&
      Cookies.getJSON(cookieSelector.settings).cur !== Cookies.get('new_currency')
    ) {
      return Cookies.get('new_currency');
    }

    return theme.currency.toUpperCase();
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    convert,
    convertAll,
    fetchCurrencyRates,
    getDefaultCurrencyCode,
    getSupportedCurrencies,
    init,
  });
};
