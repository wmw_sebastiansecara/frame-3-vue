/**
 * Helper: Redirects
 * ------------------------------------------------------------------------------
 * An event based redirect module that handles all global redirect requests.
 *
 * @namespace redirects
 */
import Cookies from 'js-cookie';
import axios from 'axios';

import {getQueryParamsObject} from '../helpers/utils';

/**
 * Instance cookies.
 */
const cookieSelector = {
  geolocation: 'redirect_geolocation',
  settings: 'redirect_store_settings',
};

/**
 * Instance events.
 */
const eventSelector = {
  matched: 'Redirect:isMatchedLocation',
  locationDetected: 'Redirect:locationDetected',
  unmatched: 'Redirect:isUnmatchedLocation',
};

/**
 * Export new redirect helper function.
 */
export default () => {

  /**
   * Initialise redirect scripts.
   * - If user's geolocation matches current store (Matched)
   * - If user doesn't match store (Unmatched)
   */
  function init() {
    fetchUserGeolocation()
      .then((response) => {
        Frame.EventBus.emit(eventSelector.locationDetected, {
          location: response,
          matchingStore: getMatchingStore(response.countryCode, response.continentCode),
        });

        if (isMatchedLocation(response)) {
          emitMatchedLocationEvents(response);
          return response;
        }

        emitUnmatchedLocationEvents(response);
        return response;
      })
      .then((response) => {
        setGeolocationCookie(response);
        return response;
      })
      .catch((err) => err);
  }

  /**
   * Fire events if user matches the store's supported locations.
   * - Set cookies if query parameters exist.
   * @param {Object} response - GeoIP response from ipstack.
   */
  function emitMatchedLocationEvents(response) {
    Frame.EventBus.emit(eventSelector.matched, response);
    const queryParameters = getQueryParamsObject();

    if (queryParameters.loc && queryParameters.lang && queryParameters.cur) {
      Cookies.set(cookieSelector.settings, queryParameters);
    }
  }

  /**
   * Emit events if user doesn't match the store's supported locations.
   * @param {Object} response - GeoIP response from ipstack.
   */
  function emitUnmatchedLocationEvents(response) {
    Frame.EventBus.emit(eventSelector.unmatched, response);
    Frame.EventBus.emit(eventSelector.locationDetected, {
      location: response,
      matchingStore: getMatchingStore(response.countryCode, response.continentCode),
    });
  }

  /**
   * Format user geolocation information from response.
   * @param {Object} response - ipstack.io successful response.
   */
  function getFormattedGeolocationData(response) {
    if (getGeolocationService() === 'maxmind') {
      return {
        continentCode: response.continent.code,
        continentName: response.continent.names.en,
        countryCode: response.country.iso_code,
        countryName: response.country.names.en,
      };
    }
    return {
      continentCode: response.continent_code,
      continentName: response.continent_name,
      countryCode: response.country_code,
      countryName: response.country_name,
    };
  }

  /**
   * Set cookie events based on request.
   * @param {Object} response - ipstack.io successful response.
   */
  function setGeolocationCookie(response) {
    Cookies.set(cookieSelector.geolocation, response, {expires: 7});
  }

  /**
   * Get geolocation cookies.
   */
  function getGeolocationCookie() {
    return Cookies.getJSON(cookieSelector.geolocation);
  }

  /**
   * Get theme geolocation service.
   * - Geolocation value defined in `theme-strings.liquid`.
   */
  function getGeolocationService() {
    return theme.features.geolocationService || 'ipstack';
  }

  /**
   * Return Geolocation data of current user.
   * - Check theme settings of geolocation service.
   * - Make request to either MaxMind or ipstack and resolve formatted data.
   */
  function fetchUserGeolocation() {
    return new Promise((resolve, reject) => {

      /**
       * Use cached geolocation data if it has already been requested.
       */
      if (typeof getGeolocationCookie() !== 'undefined') {
        resolve(getGeolocationCookie());
        return;
      }

      /**
       * Make a request to MaxMind and cache in a cookie.
       */
      if (getGeolocationService() === 'maxmind') {
        const onSuccess = (response) => {
          const formattedData = getFormattedGeolocationData(response);

          resolve(formattedData);
          return formattedData;
        };

        const onError = (error) => {
          reject(error);
        };

        geoip2.country(onSuccess, onError);
        return;
      }

      /**
       * Make a request to ipstack.io and cache in a cookie.
       */
      axios.get('https://api.ipstack.com/check?access_key=ccf65a6ba2167db16876c078bfe70aa7')
        .then((response) => {
          const formattedData = getFormattedGeolocationData(response.data);

          resolve(formattedData);
          return formattedData;
        })
        .catch((err) => reject(err));
    });
  }

  /**
   * Get the matching store based on the user's location.
   * - Will check countries then supported stores, then defaulting to continent.
   * @param {Object} response - Formatted store and redirect country codes from settings.
   */
  function getMatchingStore(countryCode, continentCode) {
    const matchingCountries = Frame.MultiStore.filter((element) => {
      return (element.storeCode === countryCode);
    });

    const matchingSupportedStores = Frame.MultiStore.filter((element) => {
      return (element.supportedStores.includes(countryCode));
    });

    const matchingContinents = Frame.MultiStore.filter((element) => {
      return (element.storeCode === continentCode);
    });

    /**
     * If there is no matching store for the country then fallback
     * to a supported store.
     */
    if (matchingSupportedStores.length > 0) {
      return matchingSupportedStores[0];
    }

    if (matchingCountries.length <= 0) {
      const foundContinents = matchingContinents.filter((element) => {
        return (!element.storeCode);
      });

      if (foundContinents.length <= 0) {
        return matchingContinents[0];
      }
      return foundContinents[0];
    }

    return matchingCountries[0];
  }

  /**
   * Check if the users geolocation matches the store's location or supported locations.
   * @param {Object} response - GeoIP response from ipstack.
   */
  function isMatchedLocation(response) {
    return (getMatchingStore(response.countryCode).storeCode === theme.store.storeCode);
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    getMatchingStore,
    init,
  });
};
