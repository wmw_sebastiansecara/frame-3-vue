/**
 * Helper: Ajax cart
 * ------------------------------------------------------------------------------
 * An event based interface to support in communicating across components
 * that sends and retrieves responses from Ajax cart interactions.
 *
 * @namespace ajaxCart
 */
import axios from 'axios';
import Cookies from 'js-cookie';
import * as cart from '@shopify/theme-cart';

import cssClasses from '../helpers/cssClasses';
import {on, serialize} from './utils';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-ajax-cart="container"]',
  addToCart: '[js-ajax-cart="addToCart"]',
  clearCart: '[js-ajax-cart="clearCart"]',
  remove: '[js-ajax-cart="remove"]',
  lineItem: '[js-ajax-cart="lineItem"]',
  form: '[js-ajax-cart="form"]',
  cartCounter: '[js-ajax-cart="cartCounter"]',
};

/**
 * Export a new AjaxCart instance
 * @param {object} config - Settings to apply across all Ajax calls to the cart.
 */
export default (config) => {

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    addToCart: [...document.querySelectorAll(selectors.addToCart)],
    cartCounter: document.querySelector(selectors.cartCounter),
    clearCart: [...document.querySelectorAll(selectors.clearCart)],
    container: [...document.querySelectorAll(selectors.container)],
  };

  /**
  * Initialise component.
  */
  function init() {
    setListeners();
    setAddToCartEvents();
    setClearCartEvents();
  }

  /**
   * Set listeners.
   */
  function setListeners() {

    /**
     * React to any item changes in the cart.
     */
    Frame.EventBus.listen([
      'AjaxCart:itemAdded',
      'AjaxCart:itemRemoved',
      'AjaxCart:itemUpdated',
      'AjaxCart:cartCleared',
    ], (response) => {
      if (theme.features.currencyConverter) {
        Frame.MultiCurrency.CurrencyConverter.convertAll(Cookies.get('new_currency'));
      }

      updateCounter();
      if (response.item_count <= 0) {
        Frame.EventBus.emit('AjaxCart:cartEmpty', response);
      }
    });

    /**
     * Global helper function to add items to cart and trigger
     * responses and linked interactions.
     */
    Frame.EventBus.listen('AjaxCart:addItem', ({id, options}) => {
      cart.addItem(id, options)
        .then((response) => {
          return Frame.EventBus.emit('AjaxCart:itemAdded', response);
        })
        .catch((error) => error);
    });

    Frame.EventBus.listen('AjaxCart:removeItem', (key) => {
      cart.removeItem(key)
        .then((response) => {
          return Frame.EventBus.emit('AjaxCart:itemRemoved', response);
        })
        .catch((error) => error);
    });
  }

  /**
  * Set click events on buttons to post form data.
  */
  function setAddToCartEvents() {
    nodeSelectors.addToCart.forEach((element) => {
      on('click', element, (event) => handleAddToCart(event));
      on('keydown', element, (event) => handleAddToCart(event));
    });
  }

  /**
   * Handle add to cart event and set loading state.
   * @param {object} event - Click and key down event.
   */
  function handleAddToCart(event) {
    if (isKeyDownEvent(event) && !isKeyPressIsEnter(event)) {
      return;
    }

    event.preventDefault();
    const target = event.currentTarget;

    setLoadingState(target);

    axios.post('/cart/add.js', serialize(target.closest('form')))
      .then((response) => {
        Frame.EventBus.emit('AjaxCart:itemAdded', response);
        setDefaultState(target);
        executeCallback();
        return response;
      })
      .catch((error) => {
        setDefaultState();
        return error;
      });
  }

  /**
   * Check if key down event.
   * @param {Event} event - Keydown event object.
   */
  function isKeyDownEvent(event) {
    return event.type === 'keydown';
  }

  /**
   * Check if the return key is pressed.
   * @param {Event} event - Key code event.
   */
  function isKeyPressIsEnter(event) {
    return event.which === 13;
  }

  /**
   * Set clear cart events if enabled.
   */
  function setClearCartEvents() {
    if (!nodeSelectors.clearCart) {
      return;
    }

    nodeSelectors.clearCart.forEach((element) => {
      on('click', element, (event) => clearItems(event));
    });
  }

  /**
   * Handle clear cart events.
   * @param {Event} event - Click/keydown event object.
   */
  function clearItems(event) {
    event.preventDefault();

    cart.clearItems()
      .then((response) => {
        Frame.EventBus.emit('AjaxCart:cartCleared', response);
        return response;
      })
      .catch((error) => {
        return error;
      });
  }

  /**
   * Update cart counter.
   */
  function updateCounter() {
    axios.get('/cart.js')
      .then((response) => {
        renderCartCounter(response.data);
        return response.data.item_count;
      })
      .catch((error) => {
        return error;
      });
  }

  /**
   * Render cart counter.
   * @param {Object} response - Cart.json data.
   */
  function renderCartCounter(response) {
    nodeSelectors.cartCounter.classList.remove(cssClasses.hidden);
    nodeSelectors.cartCounter.innerText = response.item_count;

    if (response.item_count <= 0) {
      nodeSelectors.cartCounter.classList.add(cssClasses.hidden);
    }
  }

  /**
   * Set loading state.
   * @param {object} target clicked add to cart button in DOM.
   */
  function setLoadingState(target) {
    [...nodeSelectors.addToCart].forEach((element) => {
      if (target === element) {
        element.innerText = theme.strings.addingToCart;
      }
    });
  }

  /**
  * Reset the loading states back to default.
  * @param {object} target clicked add to cart button in DOM.
  */
  function setDefaultState(target) {
    [...nodeSelectors.addToCart].forEach((element) => {
      if (target === element) {
        element.innerText = theme.strings.addToCart;
      }
    });
  }

  /**
   * Execute callback function defined in constructor.
   */
  function executeCallback() {
    return (typeof config.callback === 'function') ? config.callback() : false;
  }

  /**
   * Public interface.
   */
  return Object.freeze({
    init,
  });
};
