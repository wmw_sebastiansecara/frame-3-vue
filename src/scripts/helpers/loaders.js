/**
 * Helper: Loaders
 * -----------------------------------------------------------------------------
 * Store for loader markup
 * - https://github.com/ConnorAtherton/loaders.css
 * - https://connoratherton.com/loaders
 */
export default {
  ballPulse: `
    <div class="loader">
      <div class="ball-pulse">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  `,
};
