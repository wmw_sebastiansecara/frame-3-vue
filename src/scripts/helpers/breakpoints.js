/**
 * Helper: Breakpoints
 * -----------------------------------------------------------------------------
 * Global configuration for consistent values for media queries in scripts.
 * - These values should mirror the breakpoint values defined in `grid.scss`.
 */
export default {
  tiny: '320px',
  small: '576px',
  medium: '768px',
  large: '1024px',
  wide: '1360px',
};
