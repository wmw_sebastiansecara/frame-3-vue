/**
 * Helper: Lazysizes
 * ------------------------------------------------------------------------------
 * Loosely coupled scripts for responsive and lazy loading images.
 *
 * @namespace lazysizes
 */
import breakpoints from '../helpers/breakpoints';

import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';

window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.customMedia = {
  '--tiny': `(min-width: ${breakpoints.tiny})`,
  '--small': `(min-width: ${breakpoints.small})`,
  '--medium': `(min-width: ${breakpoints.medium})`,
  '--large': `(min-width: ${breakpoints.large})`,
  '--wide': `(min-width: ${breakpoints.wide})`,
};
