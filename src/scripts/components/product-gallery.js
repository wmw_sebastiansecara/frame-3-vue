/**
 * Product gallery
 * ------------------------------------------------------------------------------
 * An Product gallery file that contains scripts to function
 * Zoom functionality is using Drift
 * - https://github.com/imgix/drift#basic-usage
 *
 * @namespace productGallery
 */
import Drift from 'drift-zoom';
import debounce from 'lodash-es/debounce';

import Carousel from '../components/carousel';
import cssClasses from '../helpers/cssClasses';
import bind from '../mixins/bind';
import breakpoints from '../helpers/breakpoints';
import {on} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-product-gallery="container"]',
  featuredImage: '[js-product-gallery="featuredImage"]',
  thumbnail: '[js-product-gallery="thumbnail"]',
  zoomPane: '[js-product-gallery="zoomPane"]',
};

/**
 * Export a new product gallery.
 */
export default () => {

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    container: document.querySelectorAll(selectors.container),
    featuredImage: document.querySelectorAll(selectors.featuredImage),
    thumbnail: document.querySelectorAll(selectors.thumbnail),
    zoomPane: document.querySelector(selectors.zoomPane),
  };

  /**
   * Instance globals.
   */
  const featuredImages = {};
  let thumbnailNav = {};

  /**
   * Initialise component bind.
   */
  const binder = bind(nodeSelectors.container);

  /**
   * Options for Drift zoom.
   */
  const zoomOptions = {
    hoverBoundingBox: true,
    touchBoundingBox: true,
    paneContainer: nodeSelectors.zoomPane,
    touchDelay: 500,
    zoomFactor: 2,
  };

  /**
   * Initialise component.
   */
  function init() {
    setThumbnailEvents();
    setDefaultState();
    setResizeEvents();
  }

  /**
   * Set listeners.
   */
  function setThumbnailEvents() {
    [...nodeSelectors.thumbnail].forEach((element) => {
      on('click', element, (event) => handleThumbnailClick(event));
    });
  }

  /**
   * Set window resize events for responsive initialisation.
   */
  function setResizeEvents() {
    on('resize', debounce(handleResizeEvent, 250));
  }

  /**
   * Set default state based on screen size.
   */
  function setDefaultState() {
    thumbnailNav = new Carousel('.product-gallery__thumbnails-nav', {
      cellSelector: '.product-gallery__thumbnail',
    });

    if (!window.matchMedia(`(min-width: ${breakpoints.large})`).matches) {
      thumbnailNav.init();
      return;
    }

    setZoomEvents();
    binder.set();
  }


  /**
   * Handle resize events through debounce callback.
   * - Disable zoom on small devices.
   */
  function handleResizeEvent() {
    if (!window.matchMedia(`(min-width: ${breakpoints.large})`).matches) {
      thumbnailNav.init();
      destroyZoomEvents();
      binder.remove();
      return;
    }

    if (!binder.isSet()) {
      thumbnailNav.destroy();
      setZoomEvents();
    }

    binder.set();
  }

  /**
   * Set zoom events on featured Images.
   */
  function setZoomEvents() {
    [...nodeSelectors.featuredImage].forEach((element, index) => {
      featuredImages[index] = new Drift(element.querySelector('img'), zoomOptions);
    });
  }

  /**
   * Remove zoom events on featured Images.
   */
  function destroyZoomEvents() {
    Object.keys(featuredImages).forEach((key) => {
      featuredImages[key].destroy();
    });
  }

  /**
   * Handle thumbnail image click.
   * @param {Event} event - Click event on image thumbnail.
   */
  function handleThumbnailClick(event) {
    event.preventDefault();
    updateMainThumbnail(event.currentTarget.dataset.thumbnailId);
  }

  /**
   * Switch main thumbnail image.
   * @param {Number} id - Target thumbnail id to switch to.
   */
  function updateMainThumbnail(id) {
    [...nodeSelectors.featuredImage].forEach((element) => {
      if (element.dataset.imageId.includes(id)) {
        element.classList.remove(cssClasses.hidden);
        return;
      }
      element.classList.add(cssClasses.hidden);
    });
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};

