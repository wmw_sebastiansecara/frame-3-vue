/**
 * Component: Example script
 * ------------------------------------------------------------------------------
 * An example file that contains scripts to function.
 *
 * @namespace example
 */
import cssClasses from '../helpers/cssClasses';
import {on, extendDefaults} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  toggleSelector: '[js-example="toggle"]',
};

/**
 * Create a new example object.
 * @param {object} config - Settings object defined when instantiating.
 */
export default (config) => {

  /**
   * Instance default settings.
   */
  const defaults = {
    enableFeature: false,
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    toggleSelector: document.querySelector(selectors.toggleSelector),
    cells: [...document.querySelectorAll(config.cellSelector)],
  };

  /**
   * Initialise component.
   */
  function init() {
    if (settings.enableFeature) {
      setTriggerEvents();
    }
  }

  /**
   * Set trigger listeners.
   */
  function setTriggerEvents() {
    on('click', nodeSelectors.toggleSelector, (event) => {
      event.preventDefault();
      event.currentTarget.classList.add(cssClasses.active);
    });
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
