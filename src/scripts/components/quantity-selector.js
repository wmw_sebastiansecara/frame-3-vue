/**
 * Component: Quantity selector
 * ------------------------------------------------------------------------------
 * A form control component to increment/decrement a counter.
 *
 * @namespace quantitySelector
 */
import {on} from '../helpers/utils';
import bind from '../mixins/bind';

/**
 * DOM selectors.
 */
const selectors = {
  decrement: '[js-quantity-selector="decrement"]',
  increment: '[js-quantity-selector="increment"]',
  input: '[js-quantity-selector="input"]',
};

/**
 * Create a new quantitySelector object.
 * @param {string} selector - Node id selector.
 * @param {Function} callback - Callback function executed after a quantity update.
 */
export default (selector, callback = null) => {

  /**
   * Instance settings.
   */
  const namespace = selector;
  const container = document.getElementById(selector);

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    decrement: container.querySelector(selectors.decrement),
    increment: container.querySelector(selectors.increment),
    input: container.querySelector(selectors.input),
  };

  /**
   * Initialise component bind.
   */
  const binder = bind(container);

  /**
   * Initialise component.
   */
  function init() {
    if (binder.isSet()) {
      return;
    }

    setListeners();
    setTriggerEvents();
    binder.set();
  }

  /**
   * Set listeners for each instance.
   */
  function setListeners() {
    Frame.EventBus.listen(`${namespace}:update`, (value) => {
      update(value);
      executeCallback();
    });
  }

  /**
   * Set click listeners.
   */
  function setTriggerEvents() {
    on('click', nodeSelectors.increment, (event) => handleQuantityUpdate('increment', event));
    on('click', nodeSelectors.decrement, (event) => handleQuantityUpdate('decrement', event));
  }

  /**
   * Handle quantity updates.
   * @param {string} type - Quantity update type (increment/decrement).
   * @param {Event} event - Click event.
   */
  function handleQuantityUpdate(type, event) {
    event.preventDefault();
    const inputValue = nodeSelectors.input.value;
    const currentValue = (type === 'increment') ? parseInt(inputValue, 10) + 1 : parseInt(inputValue, 10) - 1;

    if (validate(currentValue)) {
      Frame.EventBus.emit(`${namespace}:update`, currentValue);
    }
  }

  /**
   * Validate input.
   * @param {Integer} value - Value to validate.
   */
  function validate(value) {
    return (!isNaN(value) && value >= 1);
  }

  /**
   * Update DOM value and execute callback.
   * @param {Integer} value - Value to replace on DOM.
   */
  function update(value) {
    nodeSelectors.input.setAttribute('value', value);
  }

  /**
   * Execute callback function defined in constructor.
   */
  function executeCallback() {
    return (typeof callback === 'function') ? callback() : false;
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
    update,
  });
};
