/**
 * Component: Currency selector
 * ------------------------------------------------------------------------------
 * A loosely coupled script to emit store data based on a country/currency dropdown
 * - Used in conjunction with the currency-selector.liquid snippet
 *
 * @namespace currencySelector
 */

import Cookies from 'js-cookie';

import {on} from '../helpers/utils';
import Currency from '../helpers/currency';
import bind from '../mixins/bind';

/**
 * DOM selectors.
 */
const selectors = {
  option: '[js-currency-selector="option"]',
};

/**
 * Export a new currency selector instance.
 */
export default (selector) => {

  /**
   * Instance globals.
   */
  const container = document.getElementById(selector);
  const namespace = selector;

  if (!container) { return false; }

  /**
   * Initialise component bind.
   */
  const binder = bind(container);

  /**
   * Initialise component.
   */
  function init() {
    if (!isCurrencySelectorEnabled()) {
      return;
    }

    if (binder.isSet()) {
      return;
    }

    renderOptions();
    setChangeEvents();
    setListeners();
    setDefaultState();

    if (!theme.features.currencyConverter) {
      Currency().init();
    }

    binder.set();
  }

  /**
   * Set change listeners.
   */
  function setChangeEvents() {
    on('change', container, () => onSelectChange());
  }

  /**
   * Set current default state based on newest currency cookie.
   */
  function setDefaultState() {
    updateSelectedOption(Cookies.get('new_currency'));
  }

  /**
   * Set global listeners.
   */
  function setListeners() {
    Frame.EventBus.listen('Currency:updated', (currencyCode) => {
      updateSelectedOption(currencyCode);
    });
  }

  /**
   * Update the selected option displayed to the selected default currency.
   * @param {String} currencyCode - Selected currency code.
   */
  function updateSelectedOption(currencyCode) {
    [...container.querySelectorAll(selectors.option)].forEach((element) => {
      if (element.value === currencyCode) {
        container.selectedIndex = element.index;
      }
    });
  }

  /**
   * Render redirect options in currency selector.
   */
  function renderOptions() {
    container.innerHTML = '';
    container.innerHTML += getCurrenciesOptionTemplate();
  }

  /**
   * Get available redirect options.
   */
  function getCurrenciesOptionTemplate() {
    return Frame.MultiCurrency.SupportedCurrencies.map((option) => {
      return `
        <option value="${option.currencyCode}"
          data-currency-code="${option.currencyCode}"
          data-currency-locale="${option.currencyLocale}"
          data-currency-name="${option.currencyName}"
          data-currency-symbol="${option.currencySymbol}"
          js-currency-selector="option"
        >
          ${option.currencyCode}
        </option>`;
    }).join('');
  }

  /**
   * Handle change event on currency selectors.
   */
  function onSelectChange() {
    Frame.EventBus.emit(`${namespace}:changed`, getCurrencyData());
  }

  /**
   * Emit currency information from selected element using Shopify Currency rate and
   * multi-currency section theme settings.
   * @return {Object}
   */
  function getCurrencyData() {
    const selectedOption = container.options[container.selectedIndex];
    return {
      el: selectedOption,
      currency: {
        code: {
          new: selectedOption.dataset.currencyCode,
          old: Cookies.get('old_currency'),
        },
        locale: selectedOption.dataset.currencyLocale,
        name: selectedOption.dataset.currencyName,
        rate: getCurrencyRate(selectedOption.value),
        symbol: selectedOption.dataset.currencySymbol,
      },
    };
  }

   /**
    * Get exchange rate from cached response.
    */
  function getExchangeRate() {

    /**
     * Resolve cookie if API has already been called previously.
     */
    if (typeof Cookies.get('exchange_rates') !== 'undefined') {
      return Cookies.getJSON('exchange_rates');
    }

    if (typeof Frame.MultiCurrency.ExchangeRate !== 'undefined') {
      return Frame.MultiCurrency.ExchangeRate;
    }

    throw new Error('Could not find an exchange rate');
  }

  /**
   * Get currency rate from Shopify.Currency object.
   * @param {String} currencyCode - Selected currency code from target.
   * @return {Object}
   */
  function getCurrencyRate(currencyCode) {
    return Object.entries(getExchangeRate()).filter((item) => {
      return (item.includes(currencyCode));
    })[0].pop();
  }

  /**
   * Check if currency selector exists on DOM.
   */
  function isCurrencySelectorEnabled() {
    return container;
  }

  /**
   * Expose public interface
   */
  return Object.freeze({
    init,
  });
};
