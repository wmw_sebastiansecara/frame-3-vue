/**
 * Carousel
 * ------------------------------------------------------------------------------
 * An example file that contains scripts to function
 *
 * @namespace carousel
 */
import Flickity from 'flickity-imagesloaded';

import {extendDefaults} from '../helpers/utils';

/**
 * Create a new carousel instance.
 * @param {String} selector - DOM selector.
 * @param {object} config - Flickity carousel settings.
 */
export default (selector, config) => {

  /**
   * Instance default settings.
   */
  const defaults = {
    autoPlay: false,
    cellAlign: 'left',
    imagesLoaded: true,
    pageDots: false,
    prevNextButtons: false,
    wrapAround: true,
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    container: document.querySelector(selector),
    cells: [...document.querySelectorAll(config.cellSelector)],
  };

  /**
   * Instance globals.
   */
  let carousel = {};

  /**
   * Initialise carousel component.
   */
  function init() {
    if (nodeSelectors.cells.length <= 1) {
      return;
    }

    carousel = new Flickity(nodeSelectors.container, {
      autoPlay: settings.autoPlay,
      cellSelector: settings.cellSelector,
      cellAlign: settings.cellAlign,
      pageDots: settings.pageDots,
      prevNextButtons: settings.prevNextButtons,
      wrapAround: settings.wrapAround,
      imagesLoaded: settings.imagesLoaded,
    });
  }

  /**
   * Trigger reload events.
   */
  function reload() {
    carousel.reloadCells();
  }

  /**
   * Trigger resize events.
   */
  function resize() {
    carousel.resize();
  }

  /**
   * Destroy instance.
   */
  function destroy() {
    if (Object.keys(carousel).length === 0) {
      return;
    }

    carousel.destroy();
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
    reload,
    resize,
    destroy,
  });
};
