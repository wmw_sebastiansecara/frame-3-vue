/**
 * Component: Modal
 * ------------------------------------------------------------------------------
 * Instantiate Micromodal and configure defaults across modal components.
 * - https://micromodal.now.sh/#configuration
 *
 * @namespace Modal
 */
import {disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock';
import * as a11y from '@shopify/theme-a11y';

import MicroModal from '../vendors/micromodal';
import {extendDefaults} from '../helpers/utils';

/**
 * Instance events.
 */
const eventSelector = {
  modalClose: 'Modal:Close',
  modalShow: 'Modal:Show',
};

/**
 * DOM selectors.
 */
const selectors = {
  body: '[js-modal="body"]',
};

/**
 * Create a new modal object.
 * @param {object} config - Settings for modal instance.
 */
export default (config) => {

  /**
   * Instance globals.
   */
  const modal = MicroModal;

  /**
   * Instance default settings.
   */
  const defaults = {
    awaitCloseAnimation: true,
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * Instantiate a new modal.
   */
  function init() {
    modal.init({
      onShow: (container) => onShowCallback(container.id),
      onClose: (container) => onCloseCallback(container.id),
    });

    unsetTabIndexOnContainer(settings.id);
    setEventBusListeners();
  }

  /**
   * Set event bus listeners.
   */
  function setEventBusListeners() {
    Frame.EventBus.listen(`${eventSelector.modalShow}:${settings.id}`, (element) => {
      disableBodyScroll(element.querySelector(selectors.body));
    });

    Frame.EventBus.listen(`${eventSelector.modalClose}:${settings.id}`, (element) => {
      enableBodyScroll(element.querySelector(selectors.body));
      clearAllBodyScrollLocks();
    });
  }

  /**
   * Force show the modal instance.
   * @param {String} id - Modal container id.
   */
  function show(id) {
    modal.show(id);
  }

  /**
   * Force close the modal instance.
   * @param {String} id - Modal container id.
   */
  function close(id) {
    modal.close(id);
  }

  /**
   * Show modal with default settings.
   * @param {String} id - Modal container id.
   */
  function onShowCallback(id) {
    setTabIndexOnContainer(id);
  }

  /**
   * Close modal with default settings.
   * @param {String} id - Modal container id.
   */
  function onCloseCallback(id) {
    unsetTabIndexOnContainer(id);
  }

  /**
   * Unset tabindex to 0 to make them un-tabbable.
   * @param {String} id - Modal container id.
   */
  function unsetTabIndexOnContainer(id) {
    const focusable = a11y.focusable(getContainerById(id));
    focusable.forEach((element) => element.setAttribute('tabindex', -1));
  }

  /**
   * Unset tabindex to 1 to make them tabbable.
   * @param {String} id - Modal container.
   */
  function setTabIndexOnContainer(id) {
    const focusable = a11y.focusable(getContainerById(id));
    focusable.forEach((element) => element.setAttribute('tabindex', 0));
  }

  /**
   * Get container based on id.
   * @param {String} id - Modal container id.
   */
  function getContainerById(id) {
    if (typeof id !== 'string') { throw new Error('Must be a valid string id'); }
    return document.getElementById(id);
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
    show,
    close,
  });
};
