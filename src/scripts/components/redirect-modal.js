/**
 * Redirect Modal
 * ------------------------------------------------------------------------------
 * Redirect modal configuration for complete theme support
 * - https://github.com/Shopify/theme-scripts/tree/master/packages/theme-sections
 *
 * @namespace redirectModal
 */
import Cookies from 'js-cookie';

import cssClasses from '../helpers/cssClasses';
import {formToJSON, getQueryParamsObject, on, serialize} from '../helpers/utils';
import Redirects from '../helpers/redirects';
import Currency from '../helpers/currency';
import Modal from './modal';

/**
 * DOM selectors.
 */
const selectors = {
  choiceContainer: '[js-redirect-modal="choiceContainer"]',
  choiceContinue: '[js-redirect-modal="choiceContinue"]',
  container: '[js-redirect-modal="container"]',
  countrySelector: '[js-redirect-modal="countrySelector"]',
  currencySelector: '[js-redirect-modal="currencySelector"]',
  footer: '[js-redirect-modal="footer"]',
  form: '[js-redirect-modal="form"]',
  languageSelector: '[js-redirect-modal="languageSelector"]',
  redirectLink: '[js-redirect-modal="redirectLink"]',
  returnButton: '[js-redirect-modal="returnButton"]',
  settingsButton: '[js-redirect-modal="settingsButton"]',
  settingsContainer: '[js-redirect-modal="settingsContainer"]',
  updateButton: '[js-redirect-modal="updateButton"]',
};

/**
 * Attribute and data selectors.
 */
const dataSelectors = {
  redirectModal: 'js-redirect-modal',
};

/**
 * Instance cookies.
 */
const cookieSelector = {
  geolocation: 'redirect_geolocation',
  modal: 'redirect_modal',
  settings: 'redirect_store_settings',
};

/**
 * Instance events.
 */
const eventSelector = {
  locationDetected: 'Redirect:locationDetected',
  matched: 'Redirect:isMatchedLocation',
  modalClose: 'Modal:Close:redirect-modal',
  redirectIcon: 'Redirect:updateIcon',
  unmatched: 'Redirect:isUnmatchedLocation',
};

/**
 * Instance settings.
 */
const settings = {
  id: 'redirect-modal',
};

/**
 * HTML placeholder strings used in language settings.
 */
const placeholder = {
  currentStoreName: '%current_store_name%',
  redirectStoreCode: '%redirect_store_code%',
  redirectStoreName: '%redirect_store_name%',
  redirectStoreUrl: '%redirect_store_url%',
};

/**
 * Create a new notification object.
 */
export default () => {

  /**
   * Instance globals.
   */
  const container = document.querySelector(selectors.container);
  const modal = new Modal({
    id: settings.id,
  });

  /**
   * Initialise section objects.
   */
  function init() {
    modal.init();
    renderTemplates();
    setEventListeners();
    setEventBusListeners();
    setDefaultState();
  }

  /**
   * Hide choice panel if user has already dismissed the modal or made their selection.
   */
  function setDefaultState() {
    const queryParameters = getQueryParamsObject();

    if (queryParameters.loc && queryParameters.lang && queryParameters.cur) {
      handleParameterEvents(queryParameters);
      return;
    }

    if (typeof Cookies.get(cookieSelector.modal) === 'undefined') {
      return;
    }

    disableChoiceCards();
  }

  /**
   * Set event listeners.
   */
  function setEventBusListeners() {
    Frame.EventBus.listen(eventSelector.modalClose, () => handleCloseEvents());
    Frame.EventBus.listen(eventSelector.matched, () => disableChoiceCards());

    Frame.EventBus.listen(eventSelector.unmatched, () => {
      handleLocationParameterEvents();

      if (typeof Cookies.get(cookieSelector.modal) === 'undefined') {
        modal.show(settings.id);
      }
    });

    Frame.EventBus.listen(eventSelector.locationDetected, (response) => {
      render(response);
      renderTemplates();
      setEventListeners();
    });
  }

  /**
   * Update choice card template if stored settings exist.
   */
  function updateChoiceContinueCard() {
    if (typeof Cookies.getJSON(cookieSelector.settings) !== 'undefined') {
      document.querySelector(selectors.choiceContinue).innerHTML = getChoiceContinueTemplate(Cookies.getJSON(cookieSelector.settings));
    }
  }

  /**
   * Get continue choice card template using stored settings.
   * @param {Object} response - store settings cookie data submitted from form.
   */
  function getChoiceContinueTemplate(response) {
    if (typeof response.loc === 'undefined') {
      return '';
    }

    return `
      <span class="choice-card__icon flag-icon flag-icon-${response.loc.toLowerCase()}"></span>
        <span class="choice-card__label">
        ${theme.redirect.choiceContinue.replace(
          new RegExp(placeholder.currentStoreName, 'g'), response.countryName,
        )}
      </span>
    `;
  }

  /**
   * Set click events that switch between the two redirect views.
   */
  function setEventListeners() {
    on('click', container, (event) => handleClickEvents(event));
    on('submit', document.querySelector(selectors.form), (event) => handleFormSubmit(event));
    on('change', document.querySelector(selectors.countrySelector), (event) => handleCountryChange(event));
  }

  /**
   * Use event delegation on user click events on modal container.
   * @param {Object} event - Click event target on container.
   */
  function handleClickEvents(event) {
    if (isContinueBrowsingButton(event.target)) {
      handleContinueClick(event);
      return;
    }

    if (isViewSettingsButton(event.target)) {
      showSettingsView();
      return;
    }

    if (isReturnButton(event.target)) {
      showChoicesView();
      return;
    }

    if (isRedirectChoice(event.target)) {
      event.preventDefault();
      handleRedirectClick(event);
    }
  }

  /**
   * Update modal template to reflect store settings in user's cookie.
   * - Replace choice card templates.
   * - Update country selector and re-render options based on selected.
   */
  function renderTemplates() {

    /**
     * FR is being used in storeCode ternary condition because an EU
     * country code does not exist in country selector.
     */
    const storeCode = (getDefaultStoreCode() === 'EU' ? 'FR' : getDefaultStoreCode());

    const matchingStore = Redirects().getMatchingStore(storeCode);
    const matchingOption = [...document.querySelector(selectors.countrySelector).children].find((element) => {
      return (element.value === storeCode);
    });
    const selectedCurrency = Currency().getDefaultCurrencyCode();

    document.querySelector(selectors.countrySelector).selectedIndex = matchingOption.index;
    updateOptions(matchingStore);
    updateChoiceContinueCard();
    updateSelectedCurrencyOption(selectedCurrency);
    updateSelectedLanguageOption();
  }

  /**
   * Handle settings form submission.
   * @param {Object} event - Form event.
   */
  function handleFormSubmit(event) {
    event.preventDefault();
    handleURLRedirection(document.querySelector(selectors.form));
  }

  /**
   * Main handler to redirect users to the correct store based on form selection.
   * - Set loading states and redirect user after form submission.
   * - Update data-store-url on form button.
   * - Use form data as query string.
   * @param {HTMLElement} form - Setting form element.
   */
  function handleURLRedirection(form) {
    const button = document.querySelector(selectors.updateButton);
    const selectedCountryCode = formToJSON(form).loc;
    const selectedCountry = getCountryNameFromCode(selectedCountryCode);

    setButtonLoadingState(button);

    /**
     * Check if user needs to be redirected to another store using current pathname.
     * - If not, then set a cookie and refresh the page.
     * - Otherwise, redirect the user to the correct store with query parameters.
     */
    window.setTimeout(() => {
      setButtonActiveState(button);
      Cookies.set(cookieSelector.modal, 'dismissed', {expires: 7});

      if (isUserRedirected(button.dataset.storeUrl)) {
        window.location = `${button.dataset.storeUrl.replace(/\/$/, '')}${window.location.pathname}?${serialize(form)}`;
        Cookies.remove(cookieSelector.settings);
        return;
      }

      Cookies.set(cookieSelector.settings, Object.assign(formToJSON(form), {countryName: selectedCountry}));
      window.location = `${button.dataset.storeUrl.replace(/\/$/, '')}${window.location.pathname}`;
    }, 1500);
  }

  /**
   * Check if user will be redirected outside of current store.
   * @param {String} redirectUrl - The url dynamically generated in the form submit button.
   */
  function isUserRedirected(redirectUrl) {
    return (window.location.origin !== redirectUrl.replace(/\/$/, ''));
  }

  /**
   * Handle change events from country selector
   * @param {Object} event - Change event from selector.
   */
  function handleCountryChange(event) {
    const selectedOption = event.target.options[event.target.selectedIndex];
    const matchingStore = Redirects().getMatchingStore(selectedOption.value);
    updateOptions(matchingStore);
  }

  /**
   * Render new options in language and currency selector.
   * @param {Object} store - Store object from multi-store config.
   */
  function updateOptions(store) {
    document.querySelector(selectors.languageSelector).innerHTML = getOptionTemplate(store.supportedLanguages.split(', '));
    document.querySelector(selectors.currencySelector).innerHTML = getOptionTemplate(store.supportedCurrencies.split(', '));
    document.querySelector(selectors.updateButton).dataset.storeUrl = store.storeUrl;
  }

  /**
   * Get option templates from array.
   * @param {Array} options - Supported options based on matched store.
   */
  function getOptionTemplate(options) {
    return options.map((option) => {
      return `
        <option value="${option}">
          ${option}
        </option>`;
    }).join('');
  }

  /**
   * Hide choice cards and return button.
   */
  function disableChoiceCards() {
    showSettingsView();
    document.querySelector(selectors.footer).classList.add(cssClasses.hidden);
  }

  /**
   * Check if click target is continue browsing choice.
   * @param {Object} target - Click target on container.
   */
  function isContinueBrowsingButton(target) {
    return (
      typeof target.attributes[dataSelectors.redirectModal] !== 'undefined' &&
      target.getAttribute(dataSelectors.redirectModal) === 'choiceContinue'
    );
  }

  /**
   * Check if click target is a view settings button.
   * @param {Object} target - Click target on container.
   */
  function isViewSettingsButton(target) {
    return (
      typeof target.attributes[dataSelectors.redirectModal] !== 'undefined' &&
      target.getAttribute(dataSelectors.redirectModal) === 'settingsButton'
    );
  }

  /**
   * Check if click target is a view choices button.
   * @param {Object} target - Click target on container.
   */
  function isReturnButton(target) {
    return (
      typeof target.attributes[dataSelectors.redirectModal] !== 'undefined' &&
      target.getAttribute(dataSelectors.redirectModal) === 'returnButton'
    );
  }

  /**
   * Check if click target is a redirect choice button.
   * @param {Object} target - Click target on container.
   */
  function isRedirectChoice(target) {
    return (
      typeof target.attributes[dataSelectors.redirectModal] !== 'undefined' &&
      target.getAttribute(dataSelectors.redirectModal) === 'redirectLink'
    );
  }

  /**
   * Handle settings view when toggled.
   */
  function showSettingsView() {
    document.querySelector(selectors.choiceContainer).classList.add(cssClasses.hidden);
    document.querySelector(selectors.settingsContainer).classList.remove(cssClasses.hidden);
  }

  /**
   * Handle settings view when toggled.
   */
  function showChoicesView() {
    document.querySelector(selectors.choiceContainer).classList.remove(cssClasses.hidden);
    document.querySelector(selectors.settingsContainer).classList.add(cssClasses.hidden);
  }

  /**
   * Handle continue button choice click.
   * @param {Event} event - Click target of continue link.
   */
  function handleContinueClick(event) {
    event.preventDefault();
    Cookies.set(cookieSelector.modal, 'dismissed', {expires: 7});
    window.setTimeout(() => disableChoiceCards(), 1500);
  }

  /**
   * Handle parameter events when url string contains all parameters.
   * @param {String} query - Query parameter string.
   */
  function handleParameterEvents(query) {
    Cookies.set(cookieSelector.settings, query);
    Cookies.set(cookieSelector.modal, 'dismissed', {expires: 7});
    disableChoiceCards();
  }

  /**
   * Handle cookie and events based on location query string.
   * - Get query parameters and update store settings cookie.
   * - Update store icon with `Redirects:updateIcon` event.
   */
  function handleLocationParameterEvents() {
    const queryParameters = getQueryParamsObject();
    if (queryParameters.loc) {
      Frame.EventBus.emit(eventSelector.redirectIcon, queryParameters.loc.toLowerCase());
      const storeSettings = Cookies.getJSON(cookieSelector.settings);
      const selectedCountryName = getCountryNameFromCode(queryParameters.loc);

      const newSettings = Object.assign(storeSettings, {
        loc: queryParameters.loc.toLowerCase(),
        countryName: selectedCountryName,
      });

      Cookies.set(cookieSelector.settings, newSettings);
    }
  }

  /**
   * Manually handle redirect link click.
   * - Use form data for redirect url query string.
   * @param {Event} event - Click target of redirect link.
   */
  function handleRedirectClick(event) {
    const countryCode = event.target.dataset.redirectStoreCode.toUpperCase();
    const params = `
      loc=${countryCode}&
      cur=${Currency().getSupportedCurrencies(countryCode).split(',')[0]}&
      lang=${getDefaultLanguageCode(countryCode)}
    `.replace(/\s/g, '');

    event.preventDefault();
    Cookies.set(cookieSelector.modal, 'dismissed', {expires: 7});
    window.location = `${event.target.href}?${params}`;
  }

  /**
   * Replace country code in title and body content.
   * Placeholder options defined in settings.
   * @param {Object} response - GeoIP response from `redirect.js`.
   */
  function render(response) {
    renderPlaceholderText(response);
  }

  /**
   * Replace location references in modal container.
   * @param {Object} response - Redirect store response defined in `redirect.js`.
   */
  function renderPlaceholderText(response) {

    /**
     * Replace choice content.
     */
    container.innerHTML = container.innerHTML.replace(
      placeholder.redirectStoreUrl, response.matchingStore.storeUrl,
    );

    container.innerHTML = container.innerHTML.replace(
      new RegExp(placeholder.redirectStoreCode, 'g'), response.location.countryCode.toLowerCase(),
    );

    /**
     * Replace current location content.
     */
    container.innerHTML = container.innerHTML.replace(
      new RegExp(placeholder.redirectStoreName, 'g'), response.location.countryName,
    );
  }

  /**
   * Handle close callback function by setting cookie.
   */
  function handleCloseEvents() {
    Cookies.set(cookieSelector.modal, 'dismissed', {expires: 7});
  }

  /**
   * Update currency selector to use default selected currency in settings.
   * @param {String} currencyCode - Currency to select.
   */
  function updateSelectedCurrencyOption(currencyCode) {
    if (!currencyCode) { return; }

    const currencySelector = document.querySelector(selectors.currencySelector);
    const selectedOption = [...currencySelector.children].find((option) => option.value === currencyCode);

    if (typeof selectedOption === 'undefined') { return; }

    document.querySelector(selectors.currencySelector).selectedIndex = selectedOption.index;
  }

  /**
   * Update language selector to use default selected language in settings.
   */
  function updateSelectedLanguageOption() {
    const selectedLanguage = getDefaultLanguageCode();
    const languageSelector = document.querySelector(selectors.languageSelector);
    const selectedOption = [...languageSelector.children].find((option) => option.value === selectedLanguage);

    if (typeof selectedOption === 'undefined') { return; }

    document.querySelector(selectors.languageSelector).selectedIndex = selectedOption.index;
  }

  /**
   * Get store code using theme settings and cookies.
   * @return {String} Store and country code (EU, GB, US, ROW).
   */
  function getDefaultStoreCode() {
    if (typeof Cookies.getJSON(cookieSelector.settings) !== 'undefined') {
      return Cookies.getJSON(cookieSelector.settings).loc.toUpperCase();
    }

    return theme.store.storeCode;
  }

  /**
   * Get country name from country selector using country code.
   * @param {String} countryCode - Country code to filter values against.
   * @return {String} Name of the country.
   */
  function getCountryNameFromCode(countryCode) {
    return [...document.querySelector(selectors.countrySelector).children].find((element) => {
      return element.value === countryCode.toUpperCase();
    }).text;
  }

  /**
   * Get default language setting.
   * @param {String} storeCode - Store code to find in multi-store configuration.
   */
  function getDefaultLanguageCode(storeCode) {
    if (storeCode) {
      const matchingStore = Redirects().getMatchingStore(storeCode);
      return matchingStore.supportedLanguages.split(',')[0];
    }

    if (typeof Cookies.getJSON(cookieSelector.settings) !== 'undefined') {
      return Cookies.getJSON(cookieSelector.settings).lang;
    }

    const languageSelector = document.querySelector(selectors.languageSelector);
    return languageSelector.options[languageSelector.selectedIndex].value;
  }

  /**
   * Update form button to use loading state and disable.
   * @param {HTMLElement} button - Targeted form button.
   */
  function setButtonLoadingState(button) {
    button.setAttribute('disabled', true);
    button.classList.add(cssClasses.disabled);
    button.innerHTML = theme.redirect.updating;
  }

  /**
   * Return form button to active state and enable.
   * @param {HTMLElement} button - Targeted form button.
   */
  function setButtonActiveState(button) {
    button.removeAttribute('disabled');
    button.classList.remove(cssClasses.disabled);
    button.innerHTML = theme.redirect.updatePreferences;
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
