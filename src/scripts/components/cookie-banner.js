/**
 * Component: Cookie Banner
 * ------------------------------------------------------------------------------
 * Simple component to remove a container on click and disable using cookies.
 *
 * @namespace cookieBanner
 */
import Cookies from 'js-cookie';

import cssClasses from '../helpers/cssClasses';
import {extendDefaults, on} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-cookie-banner="container"]',
  close: '[js-cookie-banner="close"]',
};

/**
 * Create a new notification object.
 */
export default (config) => {

  /**
   * Instance default settings.
   */
  const defaults = {
    cookie: {
      namespace: 'cookie-policy',
      value: 'accepted',
      expiry: 7,
    },
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    container: document.querySelector(selectors.container),
  };

  /**
   * Initialise component.
   */
  function init() {
    if (!nodeSelectors.container) {
      return;
    }

    if (Cookies.get(settings.cookie.namespace) === settings.cookie.value) {
      return;
    }

    setInitState();
    setTriggerEvents();
  }

  /**
   * Set listeners.
   */
  function setTriggerEvents() {
    on('click', nodeSelectors.container.querySelector(selectors.close), (event) => handleCloseEvent(event));
  }

  /**
   * Set init state.
   */
  function setInitState() {
    nodeSelectors.container.classList.add(cssClasses.active);
  }

  /**
   * Handle close event.
   * @param {Event} event - Click event.
   */
  function handleCloseEvent(event) {
    event.preventDefault();
    nodeSelectors.container.classList.remove(cssClasses.active);
    Frame.EventBus.emit('Cookie:accepted');

    Cookies.set(settings.cookie.namespace, settings.cookie.value, {expires: settings.cookie.expiry});
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
