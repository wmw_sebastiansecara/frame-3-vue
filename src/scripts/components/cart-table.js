/**
 * Cart table
 * ------------------------------------------------------------------------------
 * An event based Ajax cart table
 *
 * @namespace cartTable
 */
import axios from 'axios';
import Cookies from 'js-cookie';
import {formatMoney} from '@shopify/theme-currency';
import * as cart from '@shopify/theme-cart';

import {on} from '../helpers/utils';
import cssClasses from '../helpers/cssClasses';
import {misc as icons} from '../helpers/svg-map';
import QuantitySelector from './quantity-selector';
import FreeShippingNotification from './free-shipping-notification';

/**
 * DOM selectors.
 */
const selectors = {
  ajaxCart: 'js-ajax-cart',
  subtotal: '[js-cart-table="subtotal"]',
  total: '[js-cart-table="total"]',
  totalDiscount: '[js-cart-table="totalDiscount"]',
  quantitySelector: '[js-cart-table="quantitySelector"]',
  lineItem: '[js-ajax-cart="lineItem"]',
};

/**
 * Create new cart table object.
 */
export default (selector) => {

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    container: document.querySelector(selector),
    subtotal: document.querySelectorAll(selectors.subtotal),
    total: document.querySelector(selectors.total),
    totalDiscount: document.querySelector(selectors.totalDiscount),
  };

  /**
   * Instance globals.
   */
  let timer = {};

  /**
   * Initialise component.
   */
  function init() {
    render();
    setListener();
    setRemoveEvents(nodeSelectors.container);
  }

  /**
   * Set listeners.
   */
  function setListener() {
    Frame.EventBus.listen(['AjaxCart:itemRemoved'], () => render());
    Frame.EventBus.listen(['AjaxCart:removingItem'], (item) => setLoadingState(item));
  }

  /**
   * Render cart template.
   */
  function render() {
    Frame.EventBus.emit('Cart:updatingPrices');

    axios.get('/cart.js')
      .then((response) => {
        nodeSelectors.container.innerHTML = getCartTemplate(response.data);
        constructQuantitySelectors();
        updateCartPrices(response.data);

        if (theme.features.currencyConverter) {
          Frame.MultiCurrency.CurrencyConverter.convertAll(Cookies.get('new_currency'));
        }

        Frame.EventBus.emit('Cart:pricesUpdated', response.data);
        return response.data;
      })
      .catch((error) => {
        return error;
      });
  }

  /**
  * Get cart template
  * @param {object} data - Cart data.
  */
  function getCartTemplate(data) {
    return data.items.map((item, index) => {
      return getLineItemTemplate(item, index);
    }).join('');
  }

  /**
  * Get line item template.
  * @param {object} item - Line item data object.
  */
  function getLineItemTemplate(item, index) {
    return `
      <div class="cart-table__item ajax-cart__line-item" js-ajax-cart="lineItem" data-key="${item.key}">
        <div class="cart-table__image ajax-cart__image">
          <a href="${item.url}">
            <img src="${item.image}" alt="${item.title}">
          </a>
        </div>

        <div class="cart-table__description">
          <div class="row no-margin">
            <div class="col xs6 s7 l8">
              <div class="ajax-cart__inner">
                ${isVendorEnabled() ? getVendorTitle(item) : ''}

                <a href="${item.url}" class="ajax-cart__title">${item.product_title}</a>

                ${getVariantTitle(item)}

                <button class="ajax-cart__remove" js-ajax-cart="remove" data-key="${item.key}">
                  ${theme.strings.cart.general.remove}
                </button>
              </div>
            </div>

            <div class="col xs6 s5 l4">
              <div class="ajax-cart__price">
                <div class="cart-table__quantity-selector" data-label="Quantity">
                  <label for="Quantity-${index}" class="label visually-hidden">Quantity</label>
                  <div id="QuantitySelector-${index}" class="quantity-selector quantity-selector--small" js-cart-table="quantitySelector">
                    <div class="quantity-selector__control">
                      <button class="button quantity-selector__button" js-quantity-selector="decrement" value="decrement">
                        ${icons.minus}
                        <span class="visually-hidden">-</span>
                      </button>
                    </div>

                    <div class="quantity-selector__control">
                      <input type="number" id="Quantity-${index}" name="quantity" value="${item.quantity}" min="1" pattern="[0-9]*" class="quantity-selector__input" js-quantity-selector="input" readonly />
                    </div>

                    <div class="quantity-selector__control">
                      <button class="button quantity-selector__button" js-quantity-selector="increment" value="increment">
                        ${icons.plus}
                        <span class="visually-hidden">+</span>
                      </button>
                    </div>
                  </div>
                </div>

                <span js-currency="price" data-price=${item.price}>
                  ${getLineItemPrice(item)}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  /**
   * Check if vendor title setting is enabled.
   */
  function isVendorEnabled() {
    return JSON.parse(nodeSelectors.container.dataset.enableVendorTitle);
  }

  /**
   * Get vendor title template.
   * @param {object} item - Cart item to get vendor title from.
   */
  function getVendorTitle(item) {
    return `<p class="ajax-cart__vendor meta">${item.vendor}</p>`;
  }


  /**
   * Get variant titles.
   * @param {object} item - Line item data object.
   */
  function getVariantTitle(item) {
    return (item.variant_options.length && !item.variant_options[0].includes('Default Title')) ? `<p class="ajax-cart__variant-title caption">${item.variant_title}</p>` : '';
  }

  /**
   * Get formatted line item price.
   * @param {object} item - Line item data object.
   */
  function getLineItemPrice(item) {
    return `${formatMoney(item.price, theme.moneyFormat)}`;
  }

  /**
   * Set event delegation to remove items on container.
   * @param {HTMLElement} container - The container to apply the remove events on.
   */
  function setRemoveEvents(container) {
    on('click', container, (event) => handleRemoveEvent(event));
  }

  /**
   * Handle remove events.
   * @param {Event} event - Click event.
   */
  function handleRemoveEvent(event) {
    if (!isTargetRemove(event.target)) {
      return;
    }

    event.preventDefault();
    Frame.EventBus.emit('AjaxCart:removingItem', event.target.closest(selectors.lineItem));

    cart.removeItem(event.target.dataset.key)
      .then((response) => {
        Frame.EventBus.emit('AjaxCart:itemRemoved', response);
        return response;
      })
      .catch((error) => {
        return error;
      });
  }

  /**
   * Detect remove target click.
   * @param {HTMLElement} target - Clicked target to check remove attributes against.
   */
  function isTargetRemove(target) {
    return (
      typeof target.attributes[selectors.ajaxCart] !== 'undefined' &&
      target.attributes[selectors.ajaxCart].nodeValue === 'remove'
    );
  }

  /**
   * Instantiate quantity selectors using id.
   * - Callback has a timer to reduce number of calls.
   */
  function constructQuantitySelectors() {
    const quantitySelectors = document.querySelectorAll(selectors.quantitySelector);

    [...quantitySelectors].forEach((item) => {
      QuantitySelector(`${item.id}`, () => {
        window.clearTimeout(timer);
        timer = window.setTimeout(() => {
          handleQuantityEvent(item);
        }, 250);
      }).init();

    });
  }

  /**
   * Handle quantity updates.
   * @param {HTMLElement} element - Quantity selector node element.
   */
  function handleQuantityEvent(element) {
    Frame.EventBus.emit('Cart:updatingPrices');

    const data = {
      key: element.closest(selectors.lineItem).dataset.key,
      quantity: parseInt(element.querySelector('input').value, 10),
    };

    cart.updateItem(data.key, {quantity: data.quantity})
      .then((response) => {
        updateCartPrices(response);

        if (theme.features.freeShippingNotification) {
          FreeShippingNotification().update(response.total_price);
        }

        Frame.EventBus.emit('Cart:pricesUpdated');
        Frame.EventBus.emit('AjaxCart:itemUpdated');

        return response;
      })
      .catch((error) => {
        return error;
      });
  }

  /**
   * Update subtotal and discount total.
   * @param {object} data - Cart response data.
   */
  function updateCartPrices(data) {
    updateSubtotal(data.items_subtotal_price);
    updateDiscountTotal(data.total_discount);
    updateTotal(data.total_price);
  }

  /**
  * Update subtotal.
  * @param {Number} price - Total subtotal price number.
  */
  function updateSubtotal(price) {
    [...nodeSelectors.subtotal].forEach((element) => {
      element.setAttribute('data-price', price);
      element.innerHTML = formatMoney(price, theme.moneyFormat);
    });
  }

  /**
  * Update total.
  * @param {Number} price - Total cart price number.
  */
  function updateTotal(price) {
    nodeSelectors.total.setAttribute('data-price', price);
    nodeSelectors.total.innerHTML = formatMoney(price, theme.moneyFormat);
  }

  /**
   * Update discount total.
   * @param {Number} price - Total discount price number.
   */
  function updateDiscountTotal(price) {
    if (!isCartDiscounted(price)) {
      hideDiscountLabel();
      return;
    }
    showDiscountLabel();
    nodeSelectors.totalDiscount.setAttribute('data-price', price);
    nodeSelectors.totalDiscount.innerHTML = formatMoney(price, theme.moneyFormat);
  }

  /**
   * Set loading state.
   * @param {object} element - Line item row in DOM.
   */
  function setLoadingState(element) {
    Frame.EventBus.emit('Cart:updatingPrices');
    element.classList.add(cssClasses.isRemoving);
  }

  /**
   * Check discount total.
   * @param {Number} total - Total discount price number.
   */
  function isCartDiscounted(total) {
    return total > 0;
  }

  /**
   * Hide discount label.
   */
  function hideDiscountLabel() {
    nodeSelectors.totalDiscount.parentElement.classList.add(cssClasses.hidden);
  }

  /**
   * Show discount label.
   */
  function showDiscountLabel() {
    nodeSelectors.totalDiscount.parentElement.classList.remove(cssClasses.hidden);
  }

  return Object.freeze({
    init,
  });
};
