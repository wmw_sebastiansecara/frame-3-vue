/**
 * Component: Sort By
 * ------------------------------------------------------------------------------
 * A select menu that changes the query string of the URL to filter collections by.
 *
 * @namespace sortBy
 */
import {on} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  sortSelector: '[js-collection="sortBy"]',
};

/**
 * Create a new sort by filter.
 */
export default () => {

  /**
   * Node selectors.
   */
  const selectMenu = document.querySelector(selectors.sortSelector);

  /**
   * Global instances.
   */
  const searchParams = new URLSearchParams(window.location.search);

  /**
   * Initialise sort by component.
   */
  function init() {
    setSelectEvents();
    if (window.location.search) {
      updateSortBy();
    }
  }

  /**
   * Set sort by select events.
   */
  function setSelectEvents() {
    on('change', selectMenu, (event) => handleChangeEvent(event));
  }

  /**
   * Handle event from select menu.
   * @param {Event} event - Change event.
   */
  function handleChangeEvent(event) {
    event.preventDefault();

    const newSearchParams = new URLSearchParams();
    newSearchParams.set(event.currentTarget.name, event.currentTarget.value);
    window.location = `${window.location.origin}${window.location.pathname}?sort_by${newSearchParams}`;
  }

  /**
   * Update default state of selector by using selectedIndex.
   */
  function updateSortBy() {
    selectMenu.options.selectedIndex = getSelectedOption().index;
  }

  /**
   * Get selected option from search parameters in URL string.
   */
  function getSelectedOption() {
    return [...selectMenu.options].filter((element) => {
      return (element.value.match(searchParams.get('sort_by')));
    })[0];
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
