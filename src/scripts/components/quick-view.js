/**
 * Component: Quick view
 * ------------------------------------------------------------------------------
 * An quick view file that contains scripts to function.
 *
 * @namespace quickView
 */
import axios from 'axios';
import Cookies from 'js-cookie';

import loaders from '../helpers/loaders';
import {on, serialize} from '../helpers/utils';
import Modal from '../components/modal';
import ProductForm from '../components/product-form';
import cssClasses from '../helpers/cssClasses';

/**
 * DOM selectors.
 */
const selectors = {
  addToCart: '[js-quick-view="addToCart"]',
  container: '[js-quick-view="container"]',
  viewport: '[js-quick-view="viewport"]',
  toggleSelectors: '[js-quick-view="toggle"]',
  body: '[js-quick-view="body"]',
};

/**
 * Attribute and data selectors.
 */
const dataSelectors = {
  quickView: 'js-quick-view',
};

/**
 * Create a new quick view object.
 * @param {object} config - Settings object defined when instantiating.
 */
export default () => {

  const namespace = 'quick-view';

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    body: document.querySelector(selectors.body),
    container: document.querySelector(selectors.container),
    toggleSelectors: document.querySelector(selectors.toggleSelectors),
    viewport: document.querySelector(selectors.viewport),
  };

  /**
   * Global instances.
   */
  const modal = new Modal({
    id: namespace,
  });

  /**
   * Initialise component.
   */
  function init() {
    setTriggerEvents();
    modal.init();
  }

  /**
   * Set trigger listeners.
   */
  function setTriggerEvents() {
    on('click', nodeSelectors.container, (event) => handleQuickViewClick(event));
  }

  /**
   * Handle quick view toggle click event.
   * @param {Event} event - Click event.
   */
  function handleQuickViewClick(event) {
    if (!isTargetToggle(event.target)) {
      return;
    }

    event.preventDefault();
    setLoadingState();
    toggleQuickView(event.target);
  }

  /**
   * Detect quick view toggle click.
   * @param {HTMLElement} target - Clicked target to check quick view attributes against.
   */
  function isTargetToggle(target) {
    return (
      typeof target.attributes[dataSelectors.quickView] !== 'undefined' &&
      target.attributes[dataSelectors.quickView].nodeValue === 'toggle'
    );
  }

  /**
   * Toggle quick view modal after fetching data from toggle target.
   * @param {HTMLElement} element- Quick view button element.
   */
  function toggleQuickView(element) {
    modal.show(namespace);

    axios.get(getProductUrl(element))
      .then((response) => {
        nodeSelectors.body.innerHTML = response.data;
        removeLoadingState();
        initProductForm();
        setAddToCartEvents();
        return response;
      })
      .catch((error) => error);
  }

  /**
   * Set loading state.
   */
  function setLoadingState() {
    nodeSelectors.viewport.classList.add(cssClasses.loading);
    nodeSelectors.body.innerHTML = loaders.ballPulse;
  }

  /**
   * Remove loading state.
   */
  function removeLoadingState() {
    nodeSelectors.viewport.classList.remove(cssClasses.loading);
  }

  /**
   * Initialise product form.
   */
  function initProductForm() {
    ProductForm({
      container: 'quick-view',
      enableSwatches: true,
    }).init();

    if (theme.features.currencyConverter) {
      Frame.MultiCurrency.CurrencyConverter.convertAll(Cookies.get('new_currency'));
    }
  }

  /**
   * Set click events on buttons to post form data.
   */
  function setAddToCartEvents() {
    on('click', document.querySelector(selectors.addToCart), (event) => handleAddToCart(event));
  }

  /**
   * Handle add to cart event and set loading state.
   * @param {object} event - Click and key down event.
   */
  function handleAddToCart(event) {
    event.preventDefault();
    const target = event.currentTarget;
    setAddingToCartState(target);

    axios.post('/cart/add.js', serialize(target.closest('form')))
      .then((response) => {
        setSuccessState(target, response);
        return response;
      })
      .catch((error) => {
        return error;
      });
  }

  /**
   * Set success state when item has been successfully added.
   * @param {HTMLElement} target - Add to cart button.
   * @param {Object} response - Successful JSON response from add.js.
   */
  function setSuccessState(target, response) {
    setDefaultState(target);

    window.setTimeout(() => {
      Frame.EventBus.emit('AjaxCart:itemAdded', response);
    }, 500);

    modal.close(namespace);
  }

  /**
   * Reset the loading states back to default.
   * @param {object} target clicked add to cart button in DOM.
   */
  function setDefaultState(target) {
    target.innerText = theme.strings.addToCart;
  }

  /**
   * Set adding state to add to cart button.
   * @param {object} target clicked add to cart button in DOM.
   */
  function setAddingToCartState(target) {
    target.innerText = theme.strings.addingToCart;
  }

  /**
   * Get quick view product template url.
   * @param {HTMLElement} element- Quick view button element.
   */
  function getProductUrl(element) {
    return `${element.dataset.productUrl}?view=quick-view`;
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
