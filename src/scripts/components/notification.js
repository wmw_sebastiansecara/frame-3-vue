/**
 * Component: Notification
 * ------------------------------------------------------------------------------
 * Simple component to remove a container on click and disable using cookies.
 *
 * @namespace example
 */
import Cookies from 'js-cookie';

import cssClasses from '../helpers/cssClasses';
import {extendDefaults, on} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-notification="container"]',
  close: '[js-notification="close"]',
};

/**
 * Create a new notification object.
 */
export default (config) => {

  /**
   * Instance default settings.
   */
  const defaults = {
    enableCookies: false,
    cookie: {
      expiry: 7,
    },
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    container: document.querySelector(config.container),
  };

  /**
   * Initialise component.
   */
  function init() {
    if (!nodeSelectors.container) {
      return;
    }

    if (settings.enableCookies && isCookieSet()) {
      return;
    }
    setInitState();
    setTriggerEvents();
  }

  /**
   * Set listeners.
   */
  function setTriggerEvents() {
    on('click', nodeSelectors.container.querySelector(selectors.close), (event) => handleCloseEvent(event));
  }

  /**
   * Set init state.
   */
  function setInitState() {
    nodeSelectors.container.classList.add(cssClasses.active);
  }

  /**
   * Handle close event.
   * @param {Event} event - Click event.
   */
  function handleCloseEvent(event) {
    event.preventDefault();
    nodeSelectors.container.classList.remove(cssClasses.active);
    Frame.EventBus.emit('Notification:dismissed');

    if (settings.enableCookies && isCookieDefined()) {
      Cookies.set(settings.cookie.namespace, settings.cookie.value, {expires: settings.cookie.expiry});
    }
  }

  /**
   * Check if cookie properties are defined.
   */
  function isCookieDefined() {
    return (
      typeof settings.cookie.namespace === 'string' &&
      typeof settings.cookie.value === 'string'
    );
  }

  /**
   * Cookies are set.
   */
  function isCookieSet() {
    return typeof Cookies.get(settings.cookie.namespace) !== 'undefined';
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
