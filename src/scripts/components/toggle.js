/**
 * Component: Toggle
 * ------------------------------------------------------------------------------
 * A accessible function to trigger a toggle-able component with a window overlay.
 *
 * @namespace Toggle
 *
 */
import {disableBodyScroll, enableBodyScroll} from 'body-scroll-lock';
import * as a11y from '@shopify/theme-a11y';

import {extendDefaults, on, isElement} from '../helpers/utils';
import bind from '../mixins/bind';
import cssClasses from '../helpers/cssClasses';
import overlay from '../mixins/overlay';

/**
 * Export default toggle module.
 */
export default (config) => {

  /**
   * Initialise component bind to prevent double binds.
   */
  const binder = bind(document.documentElement, {
    className: 'esc-bind',
  });

  /**
   * Instance globals.
   */
  let previouslySelectedElement = {};

  /**
   * Instance default settings.
   */
  const defaults = {
    focusInput: true,
    namespace: config.toggleSelector,
    overlay: true,
    scrollLock: false,
    toggleTabIndex: true,
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);
  const namespace = settings.namespace;

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    toggleSelector: document.querySelectorAll(`[js-toggle="${config.toggleSelector}"]`),
  };

  /**
   * Initiate component.
   */
  function init() {
    setToggleEvents();
    setListeners();
    setKeyDownToggleEvents();
  }

  /**
   * Set component listeners.
   */
  function setListeners() {
    Frame.EventBus.listen([
      'EscEvent:on',
      `Overlay:${namespace}:close`,
    ], (response) => {
      if (typeof response !== 'undefined' && response.selector) {
        closeToggleTarget(getTargetOfToggle(response.selector));
        return;
      }

      closeToggleTarget(getTargetOfToggle(namespace));
    });
  }

  /**
   * Set click events on toggle selectors.
   */
  function setToggleEvents() {
    [...nodeSelectors.toggleSelector].forEach((element) => {
      const target = document.getElementById(element.dataset.target);

      on('click', element, (event) => handleToggleEvent(event, target));

      if (settings.toggleTabIndex) {
        unsetTabIndexOnTarget(target);
      }
    });
  }

  /**
   * Get toggle target from selector.
   * @param {String} selector - The id of the toggle element.
   */
  function getTargetOfToggle(selector) {
    const toggleElement = document.querySelector(`[js-toggle="${selector}"]`);
    return document.getElementById(toggleElement.dataset.target);
  }

  /**
   * Handle toggle events.
   * @param {Event} event Click event.
   */
  function handleToggleEvent(event, target) {
    event.preventDefault();
    toggle(target);
  }

  /**
   * Toggle target component.
   * @param {Object} target - The clicked toggle element.
   */
  function toggle(target) {
    return isTargetActive(target) ? closeToggleTarget(target) : openToggleTarget(target);
  }

  /**
   * Open target component and fire global open event.
   * @param {Object} target - The clicked toggle element.
   */
  function openToggleTarget(target) {
    target.classList.add(cssClasses.active);

    if (settings.overlay) {
      overlay({namespace}).open();
    }

    if (settings.scrollLock) {
      disableBodyScroll(target);
    }

    Frame.EventBus.emit(`Toggle:${namespace}:open`, target);
    focusTarget(target);

    if (settings.toggleTabIndex) {
      setTabIndexOnTarget(target);
    }

    binder.set();
  }

  /**
   * Close target component and fire global close event.
   * @param {Object} target - The clicked toggle element.
   */
  function closeToggleTarget(target) {
    if (!target || !isTargetActive(target)) { return; }

    target.classList.remove(cssClasses.active);

    if (settings.overlay) {
      overlay({namespace}).close();
    }

    if (settings.scrollLock) {
      enableBodyScroll(target);
    }

    Frame.EventBus.emit(`Toggle:${namespace}:close`);
    removeFocusTarget();

    if (settings.toggleTabIndex) {
      unsetTabIndexOnTarget(target);
    }

    binder.remove();
  }

  /**
   * Check if target is active.
   * @param {Object} target - The clicked toggle element.
   */
  function isTargetActive(target) {
    return target.classList.contains(cssClasses.active);
  }

  /**
   * Set a11y focus on first target.
   * @param {Object} target - The clicked toggle element.
   */
  function focusTarget(target) {
    if (!target) {
      return;
    }

    /**
     * Accessibility helpers to keep track of focused and focusable elements.
     */
    previouslySelectedElement = document.activeElement;
    const focusableElements = a11y.focusable(target);

    if (focusableElements.length) {
      a11y.trapFocus(target, {elementToFocus: focusableElements[0]});
      return;
    }

    a11y.trapFocus(target);
  }

  /**
   * Remove a11y focus on target.
   */
  function removeFocusTarget() {
    if (isElement(previouslySelectedElement)) {
      window.setTimeout(() => previouslySelectedElement.focus(), 0);
    }

    a11y.removeTrapFocus();
  }

  /**
   * Set tabindex to 0 on target focusables to make them un-tabbable.
   * @param {String} target - Target element.
   */
  function unsetTabIndexOnTarget(target) {
    const focusable = a11y.focusable(target);
    focusable.forEach((element) => element.setAttribute('tabindex', -1));
  }

  /**
   * Unset tabindex on target focusables to 1 to make them tabbable.
   * @param {String} target - Target element.
   */
  function setTabIndexOnTarget(target) {
    const focusable = a11y.focusable(target);
    focusable.forEach((element) => element.setAttribute('tabindex', 0));
  }

  /**
   * Set Esc keyboard event.
   * @param {Event} event - Key code event set on document.
   */
  function onEscEvent(event) {
    if (!isKeyPressIsEsc(event) || !binder.isSet()) {
      return;
    }

    Frame.EventBus.emit('EscEvent:on');
    binder.remove();
  }

  /**
   * Check if Esc key has been pressed.
   * @param {Event} event - Keyboard event passed from user input on document.
   */
  function isKeyPressIsEsc(event) {
    return event.keyCode === 27;
  }

  /**
   * Listen for keydown events on document.
   */
  function setKeyDownToggleEvents() {
    if (binder.isSet()) {
      return;
    }

    on('keydown', (event) => onEscEvent(event));
    binder.set();
  }

  /**
   * Return immutable object.
   */
  return Object.freeze({
    init,
    open: openToggleTarget,
    close: closeToggleTarget,
  });
};
