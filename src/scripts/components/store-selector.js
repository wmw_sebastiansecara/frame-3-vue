/**
 * Component: store selector
 * ------------------------------------------------------------------------------
 * A loosely coupled script to emit store data based on a store dropdown
 * - Used in conjunction with the store-selector.liquid snippet
 *
 * @namespace storeSelector
 */
import {on} from '../helpers/utils';
import bind from '../mixins/bind';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-store-selector="container"]',
  selector: '[js-store-selector="selector"]',
};

/**
 * Export a new store selector instance.
 */
export default () => {

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    container: document.querySelectorAll(selectors.selector) || null,
  };

  /**
   * Initialise component bind.
   */
  const binder = bind(nodeSelectors.container);

  /**
   * Initialise component.
   */
  function init() {
    if (!isStoreSelectorEnabled()) {
      return;
    }

    if (binder.isSet()) {
      return;
    }

    renderOptions();
    setTriggerEvents();
    binder.set();
  }

  /**
   * Set listeners.
   */
  function setTriggerEvents() {
    [...nodeSelectors.container].forEach((element) => {
      on('change', element, () => onSelectChange(element));
    });
  }

  /**
   * Handle change event on Store selectors.
   * @param {HTMLElement} element - Selected drop-down.
   */
  function onSelectChange(element) {
    if (getRedirectURL(element.value)) {
      window.location = getRedirectURL(element.value);
    }
  }

  /**
   * Render redirect options in country selector.
   */
  function renderOptions() {
    const template = getRedirectOptionTemplate();

    [...nodeSelectors.container].forEach((element) => {
      element.innerHTML += template;
    });
  }

  /**
   * Get available redirect options.
   */
  function getRedirectOptionTemplate() {
    return Frame.MultiStore.map((option) => {
      return `
        <option value="${option.storeCode}" data-store-code="${option.storeCode}">
          ${option.storeName}
        </option>`;
    }).join('');
  }

  /**
   * Get redirect URL defined in redirect.js.
   * @param {String} storeCode - Store code.
   */
  function getRedirectURL(storeCode) {
    const matchingStore = Frame.MultiStore.filter((element) => (element.storeCode === storeCode))[0];
    return matchingStore ? matchingStore.storeUrl : null;
  }

  /**
   * Check if country selector exists on DOM.
   */
  function isStoreSelectorEnabled() {
    return nodeSelectors.container;
  }

  /**
   * Expose public interface
   */
  return Object.freeze({
    init,
  });
};
