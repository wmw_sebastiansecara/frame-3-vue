/**
 * Product swatches
 * ------------------------------------------------------------------------------
 * A tightly coupled script to handle swatch selections on a product form.
 * - Support for multi-tiered variants.
 * - Dynamic inventory checking on option selection.
 * - Disabling is based on the most recently selected option.
 *
 * @namespace productSwatches
 */
import {on, extendDefaults, unique, difference, getSiblings} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-product-swatches="container"]',
  option: '[js-product-swatches="option"]',
  label: '[js-product-swatches="label"]',
};

/**
 * Data selectors.
 */
const dataSelectors = {
  available: 'data-available',
  disable: 'disabled',
  productSwatch: 'js-product-swatches',
  progress: 'data-progress',
};

/**
 * Export a new product swatch object.
 */
export default (config) => {

  /**
   * Instance globals.
   */
  const productData = (typeof config.data === 'undefined') ? JSON.parse(document.getElementById('product').innerHTML) : config.data;

  /**
   * Node selectors.
   */
  const nodeSelectors = {
    container: document.querySelectorAll(selectors.container),
    option: document.querySelectorAll(selectors.option),
  };

  /**
   * Instance default settings.
   */
  const defaults = {
    emptyState: false,
  };

  /**
   * Instance settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * Initialise component.
   */
  function init() {
    setListeners();
    setInitState();
    setToggleEvents();

    if (settings.emptyState) {
      setEmptyState();
      window.setTimeout(() => { Frame.EventBus.emit('ProductSwatches:reset'); }, 0);
    } else {
      window.setTimeout(() => { setInitialStateManually(); }, 0);
    }
  }

  /**
   * Set event bus listeners.
   */
  function setListeners() {
    Frame.EventBus.listen('ProductForm:change', (response) => handleProductFormChange(response));
  }

  /**
   * Set init state.
   */
  function setInitState() {
    disableUnavailableOptions();
  }

  /**
   * Set empty state prompting user to select valid inputs to add to cart.
   */
  function setEmptyState() {
    [...nodeSelectors.option].forEach((option) => {
      option.querySelector('input').checked = false;
    });
  }

  /**
   * Set events on node elements.
   */
  function setToggleEvents() {
    [...nodeSelectors.container].forEach((element) => {
      on('click', element, (event) => handleOptionClick(event));
      on('keydown', element, (event) => handleOptionKeydown(event));
    });
  }

  /**
   * Handle click events on the container.
   * @param {Event} event - Click event on the container.
   */
  function handleOptionClick(event) {
    if (!isTargetSwatch(event.target)) { return; }
    toggleOption(event.target);
  }

  /**
   * Handle keyboard events on the container.
   * @param {Event} event - Keyboard event on the container.
   */
  function handleOptionKeydown(event) {
    if (event.keyCode !== 13) { return; }
    toggleOption(event.target);
  }

  /**
   * Manual trigger of click to initiate the form state.
   */
  function setInitialStateManually() {
    if (!getFirstAvailableInput()) {
      return;
    }

    getFirstAvailableInput().querySelector(selectors.label).click();
  }

  /**
   * Handle product form changes.
   * @param {Object} response - `ProductForm:change` payload.
   */
  function handleProductFormChange(response) {
    disableUnavailableOptionsBySelection(response);
  }

  /**
   * Detect if target is a product swatch.
   * @param {HTMLElement} target - Clicked target to check swatch attributes against.
   */
  function isTargetSwatch(target) {
    return (
      typeof target.attributes[dataSelectors.productSwatch] !== 'undefined' &&
      target.attributes[dataSelectors.productSwatch].nodeValue === 'label'
    );
  }

  /**
   * Toggle selected option.
   * @param {Node} target - DOM node from clicked event.
   */
  function toggleOption(target) {
    const container = target.closest(selectors.container);
    const button = target.closest(selectors.option);

    setContainerActive(container);
    updateOptionState(button, container.dataset.index);

    // Add to callback queue to execute after call stack is empty.
    window.setTimeout(() => { emitSelectedOption(button); }, 0);
  }

  /**
   * Dispatch option selected event.
   * @param {Node} target - DOM node from clicked event.
   */
  function emitSelectedOption(target) {
    Frame.EventBus.emit('ProductSwatches:optionSelected', {
      formCompleted: getFormProgress(),
      optionSelector: target.closest(selectors.container),
      target,
    });
  }

  /**
   * Update the state of all options to match availability of current selection.
   * @param {HTMLElement} target - Clicked input element.
   * @param {Number} index - The option row that the input belongs to.
   */
  function updateOptionState(target, index) {
    const pairedVariants = getPairedVariants(target.dataset.value, index);
    const label = target.querySelector('input');

    label.checked = true;
    disablePairedVariants(pairedVariants, index);
  }


  /**
   * Set option row to active to track progress.
   * @param {Node} container - Parent DOM node from clicked target.
   */
  function setContainerActive(container) {
    container.setAttribute(dataSelectors.progress, 'active');
  }

  /**
   * Get first available swatch input.
   */
  function getFirstAvailableInput() {
    if (!nodeSelectors.option) {
      return [];
    }

    return [...nodeSelectors.option].filter((option) => (option.dataset.available === 'true'))[0];
  }

  /**
   * Get available variants.
   */
  function getAvailableVariants() {
    return productData.variants.filter((variant) => variant.available);
  }

  /**
   * Get unavailable variants.
   */
  function getUnavailableVariants() {
    return productData.variants.filter((variant) => !variant.available);
  }

  /**
   * Get current unavailable options based on selection.
   * @param {Object} response - `ProductForm:change` payload.
   */
  function getCurrentUnavailableVariants(response) {

    /**
     * Get a list of unavailable items based on the currently selected options.
     * - Join the results and only return unique items.
     */
    const currentAvailableOptions = unique([].concat(...response.currentOptions.map((option) => {
      return getUnavailableVariants().filter((element) => (element[option.index] === option.value));
    })));

    /**
     * Return variants where the selected options match the option values in product.variants data.
     */
    return currentAvailableOptions.filter((element) => {
      if (response.currentOptions.length === 1) {
        return (element[response.currentOptions[0].index] === response.currentOptions[0].value);
      }

      if (response.currentOptions.length === 2) {
        return (
          element[response.currentOptions[0].index] === response.currentOptions[0].value &&
          element[response.currentOptions[1].index] === response.currentOptions[1].value
        );
      }

      return (
        element[response.currentOptions[0].index] === response.currentOptions[0].value &&
        element[response.currentOptions[1].index] === response.currentOptions[1].value &&
        element[response.currentOptions[2].index] === response.currentOptions[2].value
      );
    });
  }

  /**
   * Get option values.
   * @param {Object} variants - product.variants object.
   * @param {String} property - Variant property to filter.
   * @return Values of variant properties.
   */
  function getVariantProperties(variants, property) {
    return variants.map((variant) => variant[property]).filter(Boolean);
  }

  /**
   * Get unavailable variants.
   * - Example: getVariantsByOption('option1', 'Black').
   * @param {String} property - Option string to return.
   * @param {String} value - Option value to compare against.
   * @returns {Object} All variants based on option value.
   */
  function getVariantsByOption(property, value) {
    return productData.variants.filter((variant) => (variant[property] === value));
  }

  /**
   * Get paired variants from selected option scoped to option selector container.
   * @param {String} value - Value of the selected option.
   * @param {Number} index - Option selector position number.
   */
  function getPairedVariants(value, index) {
    return getAvailableVariants().filter((variant) => (variant[`option${index}`] === value));
  }

  /**
   * Get current form state.
   * - Return true if all set of options have been selected.
   * @return {Boolean}
   */
  function getFormProgress() {
    if (!settings.emptyState) { return true; }
    return [...nodeSelectors.container].every((element) => (element.getAttribute(dataSelectors.progress) === 'active'));
  }

  /**
   * Disable all unavailable variant options of a single option.
   * - If all "Blacks" are sold out, disable the "Black" swatch.
   */
  function disableUnavailableOptions() {
    [...nodeSelectors.container].forEach((container) => {
      const index = container.dataset.index;

      /**
       * Check all option swatches and disable based on the `data-available` attribute value.
       */
      [...container.querySelectorAll(selectors.option)].forEach((element) => {
        const optionVariants = getVariantsByOption(`option${index}`, element.dataset.value);
        const isUnavailable = optionVariants.every((variant) => !variant.available);

        if (isUnavailable) {
          element.setAttribute(dataSelectors.available, false);
          element.querySelector(selectors.label).setAttribute(dataSelectors.disable, true);
          return;
        }

        element.setAttribute(dataSelectors.available, true);
      });
    });
  }

   /**
   * Disable unavailable options based on selected swatches.
   * @param {Object} response - ProductForm:change payload.
   */
  function disableUnavailableOptionsBySelection(response) {
    const unavailableVariants = getCurrentUnavailableVariants(response);
    const selectedOptions = response.currentOptions.map((option) => (option.index));
    const unselectedOptions = difference(selectedOptions, ['option1', 'option2', 'option3']);

    if (!unavailableVariants.length) { return; }

    /**
     * Get all unavailable options based on unselected options.
     * @returns {Array} ["28", "30", "30"]
     */
    const unavailableOptions = unavailableVariants.map((variant) => {
      return unselectedOptions.map((option) => (variant[option]));
    }).filter(Boolean);

    /**
     * Disable options when there is only one set of options left to select.
     */
    if (selectedOptions.length !== 1) {
      const unselectedSwatches = document.querySelectorAll(`[data-index="${unselectedOptions}"]`);
      const unavailableSwatches = [...unselectedSwatches].filter((element) => (unavailableOptions.flat().includes(element.value)));

      unavailableSwatches.forEach((element) => disableOptionByInput(element));
    }
  }

  /**
   * Disable the variants of the option just selected.
   * @param {Object} variants - Variants from getPairedVariants().
   * @param {Number} index - Option position number to exclude from disabling.
   */
  function disablePairedVariants(variants, index) {
    let isSoldOut = true;

    [...nodeSelectors.container].forEach((container) => {
      const optionSelectorIndex = container.dataset.index;
      const availableOptions = getVariantProperties(variants, `option${optionSelectorIndex}`);

      /**
       * Skip the selected container from being checked.
       */
      if (optionSelectorIndex === index) {
        return;
      }

      /**
       * Disable options missing from available options.
       */
      [...container.querySelectorAll(selectors.option)].forEach((element) => {
        if (
          element.dataset.available === 'false' ||
          !availableOptions.includes(element.dataset.value)
        ) {
          element.querySelector(selectors.label).setAttribute(dataSelectors.disable, true);
          return;
        }

        isSoldOut = false;
        element.querySelector(selectors.label).removeAttribute(dataSelectors.disable);
      });
    });

    if (isSoldOut) {
      Frame.EventBus.emit('ProductSwatches:soldOut');
      return;
    }

    Frame.EventBus.emit('ProductSwatches:available', {
      formCompleted: getFormProgress(),
      variants,
    });
  }

  /**
   * Disable option by using input element.
   * @param {HTMLElement} input - Swatch input element.
   */
  function disableOptionByInput(input) {
    getSiblings(input)[0].querySelector(selectors.label).setAttribute(dataSelectors.disable, true);
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
