/**
 * Component: Search bar
 * ------------------------------------------------------------------------------
 * Script to control the appearance and functionality of all search bar components.
 *
 * @namespace search-bar
 */
import cssClasses from '../helpers/cssClasses';
import {on, getSiblings} from '../helpers/utils';

/**
 * DOM selectors.
 */
const selectors = {
  form: '[js-search-bar="form"]',
  input: '[js-search-bar="input"]',
  label: '[js-search-bar="label"]',
};

/**
 * Create a new search bar object.
 */
export default () => {

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    form: document.querySelectorAll(selectors.form),
    input: document.querySelectorAll(selectors.input),
  };

  /**
   * Instance defaults.
   */
  const placeholder = document.querySelector(selectors.label).innerText.trim();

  /**
   * Initialise component.
   */
  function init() {
    setKeypressEvents();
    setFocusEvents();
  }

  /**
   * Set keypress events on search input to hide label.
   */
  function setKeypressEvents() {
    [...nodeSelectors.input].forEach((element) => {
      on('keydown', element, (event) => handleKeypressEvent(event));
    });
  }

  /**
   * Set focus events on search input to reset placeholder value.
   */
  function setFocusEvents() {
    [...nodeSelectors.input].forEach((element) => {
      on('blur', element, (event) => handleFocusEvent(event));
    });
  }

  /**
   * Handle search focus event.
   * @param {Event} event - blur event on search input.
   */
  function handleFocusEvent(event) {
    const target = event.target;

    if (target.value.length === 0) {
      getSiblings(target)[0].value = placeholder;
      getSiblings(target)[0].classList.remove(cssClasses.hidden);
    }
  }

  /**
   * Handle search keydown event.
   * @param {Event} event - keydown event on search input.
   */
  function handleKeypressEvent(event) {
    const target = event.target;

    if (target.value.length > 0) {
      getSiblings(target)[0].classList.add(cssClasses.hidden);
      return;
    }
    getSiblings(target)[0].classList.remove(cssClasses.hidden);
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
};
