/**
 * Component: Free shipping notification script
 * ------------------------------------------------------------------------------
 * A re-usable snippet to display the amount left to spend for free shipping.
 * - This is purely cosmetic and does not control the shipping rates in the admin.
 *
 * @namespace freeShippingNotification
 */
import axios from 'axios';
import Cookies from 'js-cookie';
import {formatMoney} from '@shopify/theme-currency';

/**
 * DOM selectors.
 */
const selectors = {
  container: '[js-free-shipping-notification="container"]',
  text: '[js-free-shipping-notification="text"]',
};

/**
 * Create a new free shipping notification object.
 * @param {object} config - Settings object defined when instantiating.
 */
export default () => {

  /**
   * DOM node selectors.
   */
  const nodeSelectors = {
    container: document.querySelectorAll(selectors.container),
    text: document.querySelectorAll(selectors.text),
  };

  /**
   * Initialise component.
   */
  function init() {
    setListeners();
  }

  /**
   * Set listeners to update on every cart change.
   */
  function setListeners() {
    Frame.EventBus.listen([
      'AjaxCart:itemAdded',
      'AjaxCart:itemRemoved',
      'AjaxCart:itemUpdated',
      'AjaxCart:cartCleared',
    ], () => update());
  }

  /**
   * Force update using arguments but with a fallback with cart data.
   * @param {Number} total - Value to compare threshold against.
   */
  function update(total) {
    if (total) {
      render(total);
      convertCurrency();
      return;
    }

    axios.get('/cart.js')
      .then((response) => {
        render(response.data.total_price);
        convertCurrency();
        return response;
      })
      .catch((error) => {
        return new Error('Could not get cart data', error);
      });
  }

  /**
   * Update currency based on selected currency.
   */
  function convertCurrency() {
    if (theme.features.currencyConverter) {
      Frame.MultiCurrency.CurrencyConverter.convertAll(Cookies.get('new_currency'));
    }
  }

  /**
   * Update notification template using new values.
   * @param {Number} total - Current cart total.
   */
  function render(total) {
    const threshold = getThreshold();
    const difference = threshold - total;
    const messages = theme.freeShipping.messages;

    if (difference <= 0) {
      [...nodeSelectors.text].forEach((element) => (element.innerHTML = messages.ideal));
      return;
    }

    if (total < threshold && total > 0) {
      [...nodeSelectors.text].forEach((element) => {
        element.innerHTML = messages.partial.replace('#value#', getValueTemplate(difference));
      });
      return;
    }

    [...nodeSelectors.text].forEach((element) => {
      element.innerHTML = messages.blank.replace('#value#', getValueTemplate(threshold));
    });
  }

  /**
   * Get value template with currency data selectors.
   * @param {Number} price - Price to store in price data attribute.
   */
  function getValueTemplate(price) {
    return `
      <span js-currency="price" data-price="${price}">${getFormattedCurrency(price)}</span>
    `;
  }

  /**
   * Get formatted currency.
   * @param {Number} cents - Raw amount to convert before formatting.
   */
  function getFormattedCurrency(cents) {
    return formatMoney(cents, theme.moneyFormat);
  }

  /**
   * Get free shipping threshold value.
   */
  function getThreshold() {
    if (typeof theme.freeShipping.threshold !== 'number') {
      return Number(theme.freeShipping.threshold);
    }
    return theme.freeShipping.threshold;
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
    update,
  });
};
