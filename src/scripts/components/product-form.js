/**
 * Component: Product form
 * ------------------------------------------------------------------------------
 * Highly coupled scripts to control the behaviour of a product form
 *
 * @namespace productForm
 */
import {formatMoney} from '@shopify/theme-currency';
import Cookies from 'js-cookie';

import {on} from '../helpers/utils';
import cssClasses from '../helpers/cssClasses';
import bind from '../mixins/bind';

import QuantitySelector from './quantity-selector';
import productSwatches from './product-swatches';

/**
 * DOM selectors.
 */
const selectors = {
  addToCart: '[js-product-form="addToCart"]',
  comparePrice: '[js-product-form="comparePrice"]',
  container: '[js-product-form="container"]',
  masterSelector: '[js-product-form="masterSelector"]',
  optionSelector: '[js-product-form="optionSelector"]',
  price: '[js-product-form="price"]',
  priceWrapper: '[js-product-form="priceWrapper"]',
  quantitySelector: '[js-quantity-selector="container"]',
};

/**
 * Create a new product form object.
 */
export default (config) => {

  /**
   * Instance settings.
   */
  const settings = {
    enableHistoryState: config.enableHistoryState || false,
  };

  /**
   * DOM node selectors.
   */
  const container = document.querySelector(`[js-product-form="${config.container}"]`);
  const nodeSelectors = {
    addToCart: container.querySelector(selectors.addToCart),
    comparePrice: container.querySelector(selectors.comparePrice),
    masterSelector: container.querySelector(selectors.masterSelector),
    optionSelector: container.querySelectorAll(selectors.optionSelector),
    price: container.querySelector(selectors.price),
    priceWrapper: container.querySelector(selectors.priceWrapper),
    quantitySelector: container.querySelector(selectors.quantitySelector),
  };

  /**
   * Initialise component bind.
   */
  const binder = bind(container);

  /**
   * Global instances.
   */
  const productDataSelector = (config.container === 'quick-view') ? 'product-quick-view' : 'product';
  const productData = Object.freeze(JSON.parse(document.getElementById(productDataSelector).innerHTML));
  let currentVariant = {};

  /**
   * Initialise component.
   */
  function init() {
    if (binder.isSet()) {
      return;
    }

    if (currentVariant) {
      currentVariant = getVariantFromOptions();
    }

    setDefaultState();
    setListeners();
    binder.set();
  }

  /**
   * Set default state.
   */
  function setDefaultState() {
    if (theme.features.productSwatches) {
      productSwatches({
        data: productData,
      }).init();
    }

    setQuantitySelectors();
  }

  /**
   * Set listeners.
   */
  function setListeners() {
    setSelectEvents();
    Frame.EventBus.listen('Variant:priceChange', (response) => onPriceChange(response));
    Frame.EventBus.listen('ProductSwatches:available', (response) => handleSwatchAvailability(response));
    Frame.EventBus.listen('ProductSwatches:optionSelected', (response) => handleOptionChange(response));
    Frame.EventBus.listen('ProductSwatches:reset', () => resetForm());
    Frame.EventBus.listen('ProductSwatches:soldOut', () => updateAddToCartState(theme.strings.soldOut));
  }

  /**
   * Set event listeners on select elements.
   */
  function setSelectEvents() {
    [...nodeSelectors.optionSelector].forEach((element) => {
      if (element.nodeName === 'SELECT') {
        on('change', element, () => handleSelectChange());
      }
    });
  }

  /**
   * Get currently selected options from product form.
   * - Works with all form elements with an option selector selector.
   * @return {array} Values of currently selected variants.
   */
  function getCurrentOptions() {
    const currentOptions = [];

    [...nodeSelectors.optionSelector].forEach((element) => {
      const type = element.getAttribute('type');
      const currentOption = {};

      if (type === 'radio' || type === 'checkbox') {
        if (element.checked) {
          currentOption.value = element.value;
          currentOption.index = element.dataset.index;

          currentOptions.push(currentOption);
        }
      } else {
        currentOption.value = element.value;
        currentOption.index = element.dataset.index;

        currentOptions.push(currentOption);
      }
    });

    return currentOptions;
  }

   /**
    * Find variant based on selected values.
    * @return {object || undefined} Found - Variant object from product.variants.
    */
  function getVariantFromOptions() {
    const selectedValues = getCurrentOptions();
    const variants = productData.variants;
    let found = false;

    variants.forEach((variant) => {
      let satisfied = true;

      selectedValues.forEach((option) => {
        if (satisfied) {
          satisfied = (option.value === variant[option.index]);
        }
      });

      if (satisfied) {
        found = variant;
      }
    });

    return found || null;
  }

  /**
   * Event handler for when a variant price changes.
   * @param {Object} response - Selected variant data.
   */
  function onPriceChange(response) {
    const form = document.querySelector(`[js-product-form="${config.container}"]`);
    const priceSelector = form.querySelector(selectors.price);

    priceSelector.setAttribute('data-price', response.price);
    priceSelector.innerText = formatMoney(response.price, theme.moneyFormat);

    if (form.querySelector(selectors.comparePrice)) {
      const comparePriceSelector = form.querySelector(selectors.comparePrice);
      comparePriceSelector.setAttribute('data-price', response.compare_at_price);

      if (compareAtPriceExist(response)) {
        comparePriceSelector.innerText = formatMoney(response.compare_at_price, theme.moneyFormat);
        comparePriceSelector.classList.remove(cssClasses.hidden);
      } else {
        comparePriceSelector.innerText = '';
        comparePriceSelector.classList.add(cssClasses.hidden);
      }
    }

    if (theme.features.currencyConverter) {
      Frame.MultiCurrency.CurrencyConverter.convertAll(Cookies.get('new_currency'));
    }
  }


  /**
   * Check if compare_at_price exists on variant change.
   * @param {Object} response - Selected variant data.
   */
  function compareAtPriceExist(response) {
    return (response.compare_at_price > 0 || response.compare_at_price !== null);
  }

  /**
   * Handle default availability.
   */
  function handleSwatchAvailability(response) {
    if (!response.formCompleted) {
      disableAddToCart(theme.strings.prompt);
    }
  }

  /**
   * Event handler for when an input changes on the form.
   * @param {Object} response - Pay load from option selected.
   */
  function handleOptionChange(response) {
    Frame.EventBus.emit('ProductForm:change', {
      currentOptions: getCurrentOptions(),
      currentVariant: (response.formCompleted ? getVariantFromOptions() : null),
    });

    if (!response.formCompleted) { return; }

    updateFormState();
  }

  /**
   * Event handler for when a select element changes on the store.
   */
  function handleSelectChange() {
    updateFormState();
  }

  /**
   * Update form state based on current selections.
   */
  function updateFormState() {
    const variant = getVariantFromOptions();

    updateAddToCartState(variant);

    if (!variant) {
      return;
    }

    updateMasterSelect(variant);
    updatePrice(variant);

    currentVariant = variant;

    if (settings.enableHistoryState) {
      updateHistoryState(variant);
    }
  }

  /**
   * Updates the DOM state of the add to cart button.
   * @param {boolean} enabled - Decides whether cart is enabled or disabled.
   * @param {string} text - Updates the text notification content of the cart.
   */
  function updateAddToCartState(variant) {
    if (variant) {
      enableAddToCart();
      nodeSelectors.priceWrapper.classList.remove(cssClasses.hidden);
    } else {
      disableAddToCart();
      nodeSelectors.priceWrapper.classList.add(cssClasses.hidden);
      nodeSelectors.addToCart.innerText = theme.strings.unavailable;
      return;
    }

    if (variant.available) {
      enableAddToCart();
      nodeSelectors.addToCart.innerText = theme.strings.addToCart;
    } else {
      disableAddToCart();
      nodeSelectors.addToCart.innerText = theme.strings.soldOut;
    }
  }

  /**
   * Trigger event when variant price changes.
   * @param {object} variant - Currently selected variant.
   */
  function updatePrice(variant) {
    Frame.EventBus.emit('Variant:priceChange', variant);
  }

  /**
   * Update hidden master select of variant change.
   * @param {object} variant - Currently selected variant.
   */
  function updateMasterSelect(variant) {
    nodeSelectors.masterSelector.value = variant.id;
  }

  /**
   * Update history state for product deep linking.
   * @param {object} variant - Currently selected variant.
   */
  function updateHistoryState(variant) {
    if (!window.history.replaceState || !variant) {
      return;
    }

    const newurl = `
      ${window.location.protocol}//${window.location.host}${window.location.pathname}?variant=${variant.id}
    `;
    window.history.replaceState({path: newurl}, '', newurl);
  }

  /**
   * Reset form and assign prompt text to add to cart button.
   */
  function resetForm() {
    disableAddToCart();

    if (productData.available) {
      nodeSelectors.addToCart.innerText = theme.strings.prompt;
    }
  }

  /**
   * Set Quantity Selector component.
   */
  function setQuantitySelectors() {
    new QuantitySelector(nodeSelectors.quantitySelector.id).init();
  }

  /**
   * Add disable state to add to cart button.
   * @param {String} label - Text to use on button.
   */
  function disableAddToCart(label = theme.strings.unavailable) {
    nodeSelectors.addToCart.setAttribute('disabled', true);
    nodeSelectors.addToCart.innerText = label;
  }

  /**
   * Set add to cart button to active.
   */
  function enableAddToCart() {
    nodeSelectors.addToCart.removeAttribute('disabled');
    nodeSelectors.addToCart.innerText = theme.strings.addToCart;
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
    getVariantFromOptions,
  });
};
