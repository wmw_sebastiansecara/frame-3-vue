/**
 * Multi-store config
 * ------------------------------------------------------------------------------
 * Configuration file used in `redirect.js` and `currency.js` handler.
 * - All properties are required, leave empty string if unused.
 */
window.Frame = window.Frame || {};
window.Frame.MultiStore = [
  {
    storeCode: 'GB',
    storeName: 'United Kingdom',
    storeUrl: 'https://frame-3.myshopify.com/',
    supportedLanguages: 'EN',
    supportedCurrencies: 'GBP, AUD, EUR, USD, CAD',
    supportedStores: '',
  },
  {
    storeCode: 'AU',
    storeName: 'Australia',
    storeUrl: 'https://frame-3-au.myshopify.com/',
    supportedLanguages: 'EN',
    supportedCurrencies: 'AUD',
    supportedStores: 'CK,FJ,FM,KI,MH,NA,NU,NZ,PW,SB,TO,TV,VU,WS',
  },
  {
    storeCode: 'US',
    storeName: 'United States',
    storeUrl: 'https://frame-3-us.myshopify.com',
    supportedLanguages: 'EN, FR',
    supportedCurrencies: 'USD, CAD',
    supportedStores: '',
  },
  {
    storeCode: 'DE',
    storeName: 'Germany',
    storeUrl: 'https://frame-3-de.myshopify.com',
    supportedLanguages: 'DE',
    supportedCurrencies: 'EUR',
    supportedStores: '',
  },
  {
    storeCode: 'EU',
    storeName: 'Europe',
    storeUrl: 'https://frame-3-eu.myshopify.com',
    supportedLanguages: 'EN, FR, ES',
    supportedCurrencies: 'EUR, SEK',
    supportedStores: 'AD,AL,AM,AT,AX,BA,BE,BG,BY,CY,CZ,DK,EE,ES,FI,FO,FR,GE,GI,GR,HR,HU,IE,IS,IT,LT,LU,LV,MC,MK,MT,NL,NO,PL,PT,RO,SE,SI,SK,SM,UA,VA',
  },
  {
    storeCode: 'ROW',
    storeName: 'Rest of World',
    storeUrl: 'https://frame-3-row.myshopify.com',
    supportedLanguages: 'EN',
    supportedCurrencies: 'USD',
    supportedStores: 'AE,AF,AG,AI,AM,AO,AQ,AR,AS,AW,AZ,BB,BD,BF,BH,BI,BJ,BM,BN,BO,BQ,BR,BS,BT,BW,BZ,CA,CF,CG,CH,CI,CL,CM,CN,CO,CR,CU,CV,DJ,DM,DO,DZ,EC,EG,EH,ER,ET,FK,GA,GD,GD,GE,GF,GH,GM,GN,GP,GQ,GT,GW,GY,HK,HN,HT,ID,IL,IN,IO,IQ,IR,JM,JO,JP,KE,KG,KH,KM,KR,KW,KY,KZ,LA,LB,LK,LR,LS,LY,MA,MG,ML,MM,MN,MO,MQ,MR,MS,MU,MV,MW,MX,MY,MZ,NA,NE,NG,NI,NP,OM,PA,PE,PG,PH,PK,PR,PY,QA,RU,RW,SA,SC,SD,SG,SL,SN,SO,SR,ST,SV,SX,SY,SZ,TD,TG,TH,TJ,TM,TN,TR,TT,TW,TZ,UG,UY,UZ,VE,VG,VI,VN,YE,ZA,ZM,ZW',
  },
];
