{
  "general": {
    "accessibility": {
      "skip_to_content": "Skip to content",
      "menu": "Menu",
      "close": "Close",
      "sitenav": {
        "main_navigation": "Main Navigation",
        "show_subnav": "Show submenu for \"{{ navigation }}\""
      }
    },
    "meta": {
      "tags": "Tagged \"{{ tags }}\"",
      "page": "Page {{ page }}"
    },
    "404": {
      "title": "404 Page Not Found",
      "subtext_html":
        "The page you requested does not exist. Click <a href=\"/collections/all\">here</a> to continue shopping."
    },
    "password_page": {
      "opening_soon": "Opening Soon",
      "spread_the_word": "Spread the word",
      "login_form_heading": "Enter store using password",
      "login_form_password_label": "Password",
      "login_form_password_placeholder": "Your password",
      "login_form_submit": "Enter",
      "signup_form_heading": "Find out when we open",
      "signup_form_email_label": "Email",
      "signup_form_email_placeholder": "Your email",
      "signup_form_submit": "Submit",
      "signup_form_success": "We will send you an email right before we open!",
      "admin_link_html":
        "Are you the store owner? <a href=\"/admin\">Log in here</a>",
      "password_link": "Enter using password",
      "powered_by_shopify_html": "This shop will be powered by {{ shopify }}"
    },
    "social": {
      "share_on_facebook": "Share",
      "share_on_twitter": "Tweet",
      "share_on_pinterest": "Pin it",
      "alt_text": {
        "share_on_facebook": "Share on Facebook",
        "share_on_twitter": "Tweet on Twitter",
        "share_on_pinterest": "Pin on Pinterest"
      }
    },
    "search": {
      "title": "Search for products on our site",
      "placeholder": "Search our store",
      "submit": "Search",
      "heading": {
        "one": "Search result",
        "other": "Search results"
      },
      "results_with_count": {
        "one": "{{ count }} result for \"{{ terms }}\"",
        "other": "{{ count }} results for \"{{ terms }}\""
      },
      "no_results_html": "Please try a different search term or go back to the <a href=\"/\">homepage</a>."
    },
    "newsletter_form": {
      "label": "Email address",
      "newsletter_email": "Join our mailing list",
      "email_placeholder": "Email address",
      "confirmation": "Thanks for subscribing",
      "submit": "Sign up"
    }
  },
  "blogs": {
    "article": {
      "author_on_date_html": "Posted by {{ author }} on {{ date }}",
      "comment_meta_html": "{{ author }} on {{ date }}",
      "read_more": "Read more",
      "read_more_title": "Read more: {{ title }}",
      "by_author": "by {{ author }}",
      "tags": "Tags",
      "related_articles_title": "Related Articles"
    },
    "comments": {
      "title": "Leave a comment",
      "name": "Name",
      "email": "Email",
      "message": "Message",
      "post": "Post comment",
      "moderated":
        "Please note, comments must be approved before they are published",
      "success_moderated":
        "Your comment was posted successfully. We will publish it in a little while, as our blog is moderated.",
      "success": "Your comment was posted successfully! Thank you!",
      "with_count": {
        "one": "{{ count }} comment",
        "other": "{{ count }} comments"
      }
    },
    "general": {
      "categories": "Categories",
      "hero_description": "Tell customers about a product, collection or brand story quis libero maecenas eget sit. Quam ut eget."
    },
    "pagination": {
      "current_page": "Page {{ current }} of {{ size }}",
      "previous": "Previous",
      "next": "Next"
    }
  },
  "cart": {
    "general": {
      "title": "Shopping Cart",
      "remove": "Remove",
      "note": "Special instructions for seller",
      "subtotal": "Subtotal",
      "total": "Total",
      "shipping": "Shipping",
      "shipping_at_checkout": "Shipping, taxes, and discounts calculated at checkout.",
      "clear": "Clear Cart",
      "update": "Update Cart",
      "review": "Review & Check out",
      "checkout": "Go to checkout",
      "empty": "Your cart is currently empty.",
      "currency_disclaimer": "Orders will be processed in %shop_currency%.",
      "cookies_required": "Enable cookies to use the shopping cart",
      "continue_browsing": "Continue browsing",
      "continue_browsing_html":
        "Continue browsing <a href=\"/collections/all\">on here</a> .",
      "item_quantity": "Item quantity",
      "savings": "You're saving",
      "discount_code": "Have a discount code? Add it in the next step."
    },
    "label": {
      "product": "Product",
      "price": "Price",
      "quantity": "Quantity",
      "total": "Total",
      "discounted_price": "Discounted price",
      "original_price": "Original price"
    },
    "free_shipping": {
      "blank": "Free delivery on orders over #value#.",
      "partial": "Spend #value# for free delivery.",
      "ideal": "You are eligible for free delivery."
    }
  },
  "collections": {
    "product": {
      "quick_view": "Quickview"
    },
    "general": {
      "no_matches": "Sorry, there are no products in this collection",
      "link_title": "Browse our {{ title }} collection",
      "showing_results": "Showing {{ page_size }} results",
      "filter_label": "Refine by"
    },
    "sorting": {
      "sort_by": "Sort by",
      "manual": "Featured",
      "price_ascending": "Price: Low to High",
      "price_descending": "Price: High to Low",
      "title_ascending": "A-Z",
      "title_descending": "Z-A",
      "created_ascending": "Oldest to Newest",
      "created_descending": "Newest to Oldest",
      "best_selling": "Best Selling"
    },
    "pagination": {
      "current_page": "Page {{ current }} of {{ size }}",
      "previous": "Previous",
      "next": "Next"
    }
  },
  "contact": {
    "form": {
      "name": "Name",
      "email": "Email",
      "phone": "Phone Number",
      "message": "Message",
      "send": "Send",
      "post_success":
        "Thanks for contacting us. We'll get back to you as soon as possible."
    }
  },
  "customer": {
    "account": {
      "title": "My Account",
      "details": "Account Details",
      "orders": "Orders",
      "view_orders": "View All Orders",
      "overview": "Account Overview",
      "recent_orders": "Recent Order",
      "addresses": "Address Book",
      "default_address": "Default Address",
      "view_addresses": "View Addresses",
      "return": "Return to Account Details",
      "return_to_orders": "Back to orders"
    },
    "activate_account": {
      "title": "Activate Account",
      "subtext": "Create your password to activate your account.",
      "password": "Password",
      "password_confirm": "Confirm Password",
      "submit": "Activate Account",
      "cancel": "Decline Invitation"
    },
    "addresses": {
      "title": "Your Addresses",
      "default": "Default Address",
      "add_new": "Add new address",
      "edit_address": "Edit address",
      "first_name": "First Name",
      "last_name": "Last Name",
      "company": "Company",
      "address1": "Address1",
      "address2": "Address2",
      "city": "City",
      "country": "Country",
      "province": "Province",
      "zip": "Postal/Zip Code",
      "phone": "Phone",
      "set_default": "Set as default address",
      "return": "Back to addresses",
      "add": "Add new address",
      "update": "Update Address",
      "cancel": "Cancel",
      "edit": "Edit Address",
      "delete": "Delete Address",
      "delete_confirm": "Are you sure you wish to delete this address?"
    },
    "login": {
      "title": "Login or create an account",
      "email": "Email",
      "password": "Password",
      "forgot_password": "Forgotten your password?",
      "sign_in": "Sign In",
      "login": "Login",
      "create": "Create an account",
      "returning_user": "Returning Customers",
      "new_customers": "New Customers",
      "create_account": "Create an account to expedite future checkouts, view and update your account details, track your order status and history.",
      "cancel": "Return to Store",
      "guest_title": "Continue as a guest",
      "guest_continue": "Continue"
    },
    "orders": {
      "title": "Your Orders",
      "order_number": "Order No.",
      "date": "Date",
      "payment_status": "Payment Status",
      "fulfillment_status": "Shipping Status",
      "total": "Total",
      "view_order": "View order",
      "none": "You haven't placed any orders yet."
    },
    "order": {
      "title": "Order {{ name }}",
      "subtitle": "Thanks for your order! Check out the details below.",
      "date_label": "Order Date",
      "date": "Placed on {{ date }}",
      "details_title": "Order Details",
      "shipping_title": "Shipping",
      "billing_title": "Billing",
      "items_title": "Order Items",
      "cancelled": "Order Cancelled on {{ date }}",
      "cancelled_reason": "Reason: {{ reason }}",
      "billing_address": "Billing Address",
      "payment_status": "Payment Status",
      "shipping_address": "Shipping Address",
      "fulfillment_status": "Shipping Status",
      "discount": "Discount",
      "shipping": "Shipping",
      "tax": "Tax",
      "product": "Product",
      "variants": "Variants",
      "sku": "SKU",
      "price": "Price",
      "quantity": "Quantity",
      "total": "Total",
      "fulfilled_at": "Date fulfilled",
      "track_shipment": "Track shipment",
      "tracking_url": "Tracking link",
      "tracking_company": "Carrier",
      "tracking_number": "Tracking number",
      "subtotal": "Subtotal"
    },
    "recover_password": {
      "title": "Reset your password",
      "email": "Email",
      "submit": "Submit",
      "cancel": "Cancel",
      "subtext": "We will send you an email to reset your password.",
      "success": "We've sent you an email with a link to update your password."
    },
    "reset_password": {
      "title": "Reset account password",
      "subtext": "Enter a new password for {{ email }}",
      "password": "Password",
      "password_confirm": "Confirm Password",
      "submit": "Reset Password"
    },
    "register": {
      "title": "Create Account",
      "first_name": "First Name",
      "last_name": "Last Name",
      "email": "Email",
      "password": "Password",
      "submit": "Register",
      "cancel": "Return to Store",
      "login_here_html": "Already have an account? <a href=\"/account/login\">log in here</a>"
    }
  },
  "homepage": {
    "onboarding": {
      "blog_title": "Your post's title",
      "blog_excerpt":
        "Your store hasn’t published any blog posts yet. A blog can be used to talk about new product launches, tips, or other news you want to share with your customers. You can check out Shopify’s ecommerce blog for inspiration and advice for your own store and blog.",
      "blog_author": "Author name",
      "product_title": "Example Product Title",
      "collection_title": "Example Collection Title",
      "no_content":
        "This section doesn’t currently include any content. Add content to this section using the sidebar."
    }
  },
  "layout": {
    "cart": {
      "title": "Cart",
      "items_count": {
        "one": "item",
        "other": "items"
      }
    },
    "customer": {
      "account": "Account",
      "logged_in_as_html": "Logged in as {{ first_name }}",
      "log_out": "Log out",
      "log_in": "Log in",
      "register": "register",
      "create_account": "Create account"
    },
    "cookie_banner": {
      "message_html": "We use own cookies to improve our services and your shopping experience. If you continue browsing your are deemed to have accepted our <a href=\"/pages/cookie-policy\">cookie policy</a>"
    },
    "footer": {
      "social_platform": "{{ name }} on {{ platform }}",
      "copyright": "Copyright",
      "payment_methods": "Payment methods accepted",
      "store_selector_error": "Unfortunately your browser is not compatible with our GeoIP detection. Please use a modern browser to continue shopping in the correct location."
    },
    "store_settings": {
      "update_preferences": "Change store preferences",
      "change_store": "Change store"
    }
  },
  "products": {
    "product": {
      "description": "Description",
      "delivery_returns": "Delivery and returns",
      "regular_price": "Regular price",
      "sold_out": "Sold Out",
      "unavailable": "Unavailable",
      "prompt": "Select options",
      "on_sale": "On Sale",
      "on_sale_from_html": "On Sale from",
      "from_text_html": "From",
      "quantity": "Quantity",
      "add_to_cart": "Add to Cart",
      "adding_to_cart": "Adding to Cart"
    }
  },
  "gift_cards": {
    "issued": {
      "title": "Here's your {{ value }} gift card for {{ shop }}!",
      "subtext": "Here's your gift card!",
      "disabled": "Disabled",
      "expired": "Expired on {{ expiry }}",
      "active": "Expires on {{ expiry }}",
      "redeem": "Use this code at checkout to redeem your gift card",
      "shop_link": "Start shopping",
      "print": "Print",
      "remaining_html": "{{ balance }} left",
      "add_to_apple_wallet": "Add to Apple Wallet",
      "initial_value": "Gift card value: {{ value }}"
    }
  },
  "date_formats": {
    "month_day_year": "%B %d, %Y",
    "day_month_year": "%d / %m / %Y"
  },
  "redirect": {
    "modal": {
      "choice_title": "Hey! We think you're browsing from %redirect_store_name%",
      "choice_text": "You will not be able to checkout in your local currency on this store. Would you like to visit our %redirect_store_name% store instead?",
      "choice_continue": "Continue browsing %current_store_name% store",
      "choice_redirect": "Yes, take me to the %redirect_store_name% store",
      "settings_title": "Store settings",
      "settings_label": "I want to choose my store settings",
      "country_selector_label": "Country",
      "currency_selector_label": "Currency",
      "language_selector_label": "Language",
      "update_preferences": "Update store preferences",
      "manual_warning": "This selection will not support checkout options based on your current location.",
      "return": "Back",
      "updating": "Updating preferences..."
    }
  }
}
